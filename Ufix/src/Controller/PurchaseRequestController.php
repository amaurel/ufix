<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\Type\NewPurchaseRequestType;
use App\Entity\Ad;
use App\Entity\PurchaseRequest;
use App\Entity\RepairProposition;
use App\Entity\PurchaseRequestState;
use Symfony\Component\Security\Core\Security;

class PurchaseRequestController extends AbstractController
{
    /** 
     * @Route("/new-purchase-request/{id}", name="new_purchase_request")
     */
    public function showNewPurchaseRequest(Request $request, Security $security, Ad $ad)
    {
        $user = $security->getUser();
        $newPurchaseRequestForm = $this->createForm(NewPurchaseRequestType::class);
        $newPurchaseRequestForm->handleRequest($request);

        if ($newPurchaseRequestForm->isSubmitted() && $newPurchaseRequestForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $data = $newPurchaseRequestForm->getData();

            $purchaseRequestStateRepository = $this->getDoctrine()->getRepository(PurchaseRequestState::class);
            $states = $purchaseRequestStateRepository->findAll();

            // if (!$ad->getPurchaseRequests()->contains($newPurchaseRequest)) {
                
            // }

            $contains = false; 
            foreach($user->getPurchaseRequests() as $rp) {
                if($rp->getAd() == $ad ) {
                    $contains = true;
                }
            }

            if(!$contains) {            
                $newPurchaseRequest = new PurchaseRequest();
                $newPurchaseRequest->setMessage($data['message']);
                $newPurchaseRequest->setAd($ad);
                $newPurchaseRequest->setBuyer($user);
                foreach($states as $s) {
                    $newPurchaseRequest->AddState($s);
                }
                $newPurchaseRequest->nextState();

                $ad->addPurchaseRequest($newPurchaseRequest);
                
                if($ad->getAdStates()[1] != $ad->getCurrentAdState()) {
                    $ad->nextState();
                }
                // dump($ad->getAdStates());
                // dump($ad->getCurrentAdState());
                // die;

                $user->addPurchaseRequest($newPurchaseRequest);

                $em->persist($user);
                $em->persist($ad);
                $em->persist($newPurchaseRequest);
                $em->flush();

                $this->addFlash('success', 'Demande d\'achat proposée !');
                return $this->render('/purchase_request/newPurchaseRequest.html.twig', [
                    'ad' => $ad,
                    'newPurchaseRequestForm' => $newPurchaseRequestForm->createView()
                ]);
            } else {
                $this->addFlash('error', 'Vous ne pouvez pas faire plusieurs demandes d\'achat pour une même annonce ! ');
                return $this->render('/purchase_request/newPurchaseRequest.html.twig', [
                    'ad' => $ad,
                    'newPurchaseRequestForm' => $newPurchaseRequestForm->createView()
                ]);
            }
        }
        return $this->render('/purchase_request/newPurchaseRequest.html.twig', [
            'ad' => $ad,
            'newPurchaseRequestForm' => $newPurchaseRequestForm->createView()
        ]);
    }
}
