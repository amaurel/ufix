<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\Type\NewRepairPropositionType;
use App\Entity\Ad;
use App\Entity\RepairProposition;
use App\Entity\RepairPropositionState;
use Symfony\Component\Security\Core\Security;

class RepairPropositionController extends AbstractController
{
 

     /** 
     * @Route("/new-repair-proposition/{id}", name="new_repair_proposition")
     */
    public function showNewRepairProposition(Request $request, Security $security, Ad $ad)
    {
        $user = $security->getUser();
        $newRepairPropositionForm = $this->createForm(NewRepairPropositionType::class);
        $newRepairPropositionForm->handleRequest($request);

        if ($newRepairPropositionForm->isSubmitted() && $newRepairPropositionForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $data = $newRepairPropositionForm->getData();
            $newRepairProposition = new RepairProposition();

            $repairPropositionStateRepository = $this->getDoctrine()->getRepository(RepairPropositionState::class);
            $states = $repairPropositionStateRepository->findAll();

            // if (!$ad->getRepairPropositions()->contains($newRepairProposition)) {
                
            // }

            $contains = false; 
            foreach($user->getRepairPropositions() as $rp) {
                if($rp->getAd() == $ad ) {
                    $contains = true;
                }
            }

            if(!$contains) {
                $newRepairProposition->setPrice($data['price']);
                $newRepairProposition->setDescription($data['description']);
                $newRepairProposition->setAd($ad);
                $newRepairProposition->setProposer($user);
                foreach($states as $s) {
                    $newRepairProposition->AddState($s);
                }
                $newRepairProposition->nextState();
                // dump($newRepairProposition);
                // die;
                $ad->addRepairProposition($newRepairProposition);
                if($ad->getAdStates()[1] != $ad->getCurrentAdState()) {
                    $ad->nextState();
                }
                
    
                $user->addRepairProposition($newRepairProposition);
    
                $em->persist($user);
                $em->persist($ad);
                $em->persist($newRepairProposition);
                $em->flush();
    
                $this->addFlash('success', 'Proposition de réparation déposée !');
                
                return $this->render('/repair_proposition/newRepairProposition.html.twig', [
                    'ad' => $ad,
                    'newRepairPropositionForm' => $newRepairPropositionForm->createView()
                ]);
            } else {
                $this->addFlash('error', 'Vous ne pouvez pas déposer plus d\'une proposition pour une annonce');
                
                return $this->render('/repair_proposition/newRepairProposition.html.twig', [
                    'ad' => $ad,
                    'newRepairPropositionForm' => $newRepairPropositionForm->createView()
                ]);
            }
            
        }
        return $this->render('/repair_proposition/newRepairProposition.html.twig', [
            'ad' => $ad,
            'newRepairPropositionForm' => $newRepairPropositionForm->createView()

        ]);
    }
}
