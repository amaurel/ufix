<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Response;
use App\Form\Type\NewUserType;

use App\Form\Type\NewAdType;
use App\Form\Type\ModifyUserType;
use App\Form\Type\NewRepairPropositionType;
use App\Entity\User;
// use App\Entity\Product;
use App\Entity\Ad;
use App\Entity\RepairProposition;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Security;
use Doctrine\ORM\EntityManagerInterface;
use App\Form\Type\ContactType;
use App\Entity\Contact;
use App\Notification\ContactNotification;
use App\Notification\SignInNotification;

/**
 * @Route("/")
 */
class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="home_page")
     */
    public function showHomePage()
    {
        // dump($request);
        // die;

        return $this->render('home.html.twig');
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        //throw new \Exception('This method can be blank - it will be intercepted by the logout key on your firewall');
        $this->addFlash('success-disconnect', 'Vous êtes déconnecté');

        return $this->redirectToRoute('home_page');
    }

    /**
     * @Route("/contact_template", name="contact_template")
     */
    public function showContactTemplate()
    {
        return $this->render('emails/signIn.html.twig');
    }

    /**
     * @Route("/login", name="login")
     */
    public function login(AuthenticationUtils $authenticationUtils, Request $request, UserPasswordEncoderInterface $passwordEncoder, SignInNotification $notification ): Response
    {
        $this->passwordEncoder = $passwordEncoder;
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        $this->addFlash('success-connect', 'Vous êtes bien connecté');

        return $this->render('login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
        ]);
    }

    /**
     * @Route("/signin", name="signin")
     */
    public function signin(AuthenticationUtils $authenticationUtils, Request $request, UserPasswordEncoderInterface $passwordEncoder, SignInNotification $notification ): Response
    {
        
        $this->passwordEncoder = $passwordEncoder;

        $newUserForm = $this->createForm(NewUserType::class);
        $newUserForm->handleRequest($request);

        if ($newUserForm->isSubmitted() && $newUserForm->isValid()) {
            // dump("ouais");
            // die;
            $em = $this->getDoctrine()->getManager();
            $data = $newUserForm->getData();
            $newUser = new User();
            //    dump($data);
            //    die;
            $newUser->setFirstName($data['firstName']);
            $newUser->setLastName($data['lastName']);
            $newUser->setEmail($data['email']);
            $newUser->setPassword($this->passwordEncoder->encodePassword($newUser, $data['password']));
            $newUser->setRoles(['ROLE_CLASSIC']);
            $newUser->setAdress($data['adress']);
            $newUser->setPostCode($data['postCode']);
            $newUser->setCountry($data['country']);
            $newUser->setCity($data['city']);
            
            $em->persist($newUser);
            $em->flush();


            $notification->notify($newUser);

            $this->addFlash('success', 'Email sent');

            return $this->redirectToRoute('login');
        }

        return $this->render('signin.html.twig', [
            'newUserForm' => $newUserForm->createView(),

        ]);
    }
}
