<?php

namespace App\Controller;

use App\Entity\Report;
use App\Form\Type\ReportType;

use App\Entity\Device;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

use App\Form\Type\NewAdType;
use App\Entity\Ad;
use App\Entity\RepairProposition;
use App\Entity\AdState;
use App\Repository\AdStateRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Security;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;


class AdController extends AbstractController
{

/**
     * @Route("/new-ad", name="new_ad")
     * @IsGranted("ROLE_USER")
     */
    public function newAd(Request $request, Security $security)
    {
        $user = $security->getUser();

        $newAdForm = $this->createForm(NewAdType::class);
        $newAdForm->handleRequest($request);

        if ($newAdForm->isSubmitted() && $newAdForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $data = $newAdForm->getData();

            $adStateRepository = $this->getDoctrine()->getRepository(AdState::class);
            $states = $adStateRepository->findByAdPurpose($data['purpose']);

            $newAd = new Ad();

            $image1 = $newAdForm->get('image1')->getData();
            $image2 = $newAdForm->get('image2')->getData();
            $image3 = $newAdForm->get('image3')->getData();

            $newAd->setDevice($data['device']);
            $newAd->setAdType(1);
            $newAd->setProductName($data['name']);
            $newAd->setProductBreakDescription($data['breakState']);
            $newAd->setProductPrice($data['price']);
            $newAd->setAdDescription($data['description']);

            $newAd->setAdPurpose($data['purpose']);
            // dump($newAd->getAdPurpose());
            // die;
            foreach($states as $s) {
                $newAd->AddAdState($s);
            }
            $newAd->nextState();
            $newAd->setOwner($user);

            

            $user->addOwnedAd($newAd);
            // $user->setFirstName("Dolan");


            $em->persist($newAd);
            $em->persist($user);
            $em->flush();


            if ($image1) {
                $newFilename = $newAd->getId().'_1.'.$image1->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $image1->move(
                        $this->getParameter('annonce_directory').'/'.$newAd->getId(),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $newAd->setImage1($newFilename);
                $em->persist($newAd);
                $em->flush();
            }

            if ($image2) {
                $newFilename = $newAd->getId().'_2.'.$image2->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $image2->move(
                        $this->getParameter('annonce_directory').'/'.$newAd->getId(),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $newAd->setImage2($newFilename);
                $em->persist($newAd);
                $em->flush();
            }

            if ($image3) {
                $newFilename = $newAd->getId().'_3.'.$image3->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $image3->move(
                        $this->getParameter('annonce_directory').'/'.$newAd->getId(),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $newAd->setImage3($newFilename);
                $em->persist($newAd);
                $em->flush();
            }

            $this->addFlash('success', 'Annonce déposée !');
            return $this->redirectToRoute('new_ad');
        }

        return $this->render('/ad/newAd.html.twig', [
            'newAdForm' => $newAdForm->createView(),
        ]);
    }

    /**
     * @Route("/ad-details/{id}", name="ad_details")
     */
    public function showAdDetails(Ad $ad, Request $request)
    {
        // dump($ad->getAdPurpose());
        //     die;
        $repository = $this->getDoctrine()->getRepository(Ad::class);
        $ads = $repository->findAll();

        // $repository = $this->getDoctrine()->getRepository(Report::class);
        // $reports = $repository->findAll();
        
        $report = new Report();
        $reportForm = $this->createForm(ReportType::class, $report);
        $reportForm->handleRequest($request);

        if ($reportForm->isSubmitted() && $reportForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            // $data = $reportForm->getData();

            $report->setAds($ad);
            $report->setCreatedAt(new \DateTime('now'));

            // $ad->addReport($report);

            // $em->persist($ad);
            $em->persist($report);
            $em->flush();
        }

        return $this->render('/ad/adDetails.html.twig', [
            'reportForm' => $reportForm->createView(),
            'ad' => $ad,
            // 'ads' => $ads,
            'reports' => $report,
        ]);
    }

    /**
     * @Route("/annonces", name="ads")
     */
    public function showAds(Security $security)
    {
        $user = $security->getUser();

        $repository = $this->getDoctrine()->getRepository(Ad::class);
        $ads = $repository->findAll();

        $deviceRepository = $this->getDoctrine()->getRepository(Device::class);
        $devices = $deviceRepository->findAll();

        return $this->render('/ad/ads.html.twig', [
            'ads' => $ads,
            'devices' => $devices,
            'user' => $user
        ]);
    }

    /** 
     * @Route("/edit-ad", name="edit_ad")
     */
    public function editAd()
    {
        return $this->render('/ad/edit_product.html.twig');
    }

    public function getAdFromDevice ($devise) {
        $deviceRepository = $this->getDoctrine()->getRepository(Device::class);
         return $deviceRepository->findAll();
    }

    /**
     * @Route("/annonces/{id}", name="ads_from_device")
     */
    public function showAdsFromDevice(Device $device)
    {
        $repository = $this->getDoctrine()->getRepository(Ad::class);
        $ads = $repository->findByDevice($device);

        $deviceRepository = $this->getDoctrine()->getRepository(Device::class);
        $devices = $deviceRepository->findAll();
        return $this->render('/ad/ads.html.twig', [
            'ads' => $ads,
            'devices' => $devices
        ]);
    }

    // /**
    //  * @Route("/{id}", name="ad_delete", methods={"DELETE"})
    //  */
    // public function delete(Request $request, Ad $ad): Response
    // {
    //     if ($this->isCsrfTokenValid('delete'.$ad->getId(), $request->request->get('_token'))) {
    //         $entityManager = $this->getDoctrine()->getManager();
    //         $entityManager->remove($ad);
    //         $entityManager->flush();
    //     }

    //     return $this->redirectToRoute('genAds');
    // }


}
