<?php

namespace App\Controller;

use Symfony\Component\Security\Core\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Entity\Ad;
use App\Repository\AdRepository;
use App\Entity\Report;
use App\Repository\ReportRepository;

class AdminController extends AbstractController
{
    /**
     * @Route("/dashboard", name="admin")
     * @IsGranted("ROLE_ADMIN")
     */
    public function index(Security $security)
    {
        $user = $security->getUser();

        return $this->render('admin/dashboard.html.twig', [
            'controller_name' => 'AdminController',
            'user' => $user,
        ]);
    }

    /**
     * @Route("/dashboard", name="dashboard")
     * @IsGranted("ROLE_ADMIN")
     */
    public function dashboard(Security $security)
    {
        $dashboard = '';
        $user = $security->getUser();

        return $this->render('admin/dashboard.html.twig', [
            'dashboard' => $dashboard,
            'user' => $user,
        ]);
    }

    /**
     * @Route("/genUsers", name="genUsers")
     * @IsGranted("ROLE_ADMIN")
     */
    public function genUsers(Security $security)
    {
        $genUsers = '';
        $user = $security->getUser();

        $repository = $this->getDoctrine()->getRepository(User::class);
        $users = $repository->findAll();

        return $this->render('admin/genUsers.html.twig', [
            'genUsers' => $genUsers,
            'user' => $user,
            'users' => $users,
        ]);
    }

    /**
     * @Route("/genAds", name="genAds")
     * @IsGranted("ROLE_ADMIN")
     */
    public function genAds(Security $security)
    {
        $genAds = '';
        $user = $security->getUser();

        $repository = $this->getDoctrine()->getRepository(Ad::class);
        $ads = $repository->findAll();

        return $this->render('admin/genAds.html.twig', [
            'genAds' => $genAds,
            'user' => $user,
            'ads' => $ads,
        ]);
    }

    /**
     * @Route("/warnAds", name="warnAds")
     * @IsGranted("ROLE_ADMIN")
     */
    public function warnAds(Security $security)
    {
        $warnAds = '';
        $user = $security->getUser();

        $repository = $this->getDoctrine()->getRepository(Report::class);
        $reports = $repository->findAll();

        return $this->render('admin/warnAds.html.twig', [
            'warnAds' => $warnAds,
            'user' => $user,
            'reports' => $reports,
        ]);
    }

    /**
     * @Route("/warnUsers", name="warnUsers")
     * @IsGranted("ROLE_ADMIN")
     */
    public function warnUsers(Security $security)
    {
        $warnUsers = '';
        $user = $security->getUser();

        return $this->render('admin/warnUsers.html.twig', [
            'warnUsers' => $warnUsers,
            'user' => $user,
        ]);
    }

    /**
     * @Route("/deleteUser/{id}", name="user_delete", methods={"DELETE"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function deleteUser(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            unlink($this->getParameter('profil_directory').'/'.$user->getProfilePicture());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('genUsers');
    }

    /**
     * @Route("/deleteAd/{id}", name="ad_delete", methods={"DELETE"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function deleteAd(Request $request, Ad $ad): Response
    {
        if ($this->isCsrfTokenValid('delete'.$ad->getId(), $request->request->get('_token'))) {
            if($ad->getImage1()){
                unlink($this->getParameter('annonce_directory').'/'.$ad->getId().'/'.$ad->getImage1());
            }
            if($ad->getImage2()){
                unlink($this->getParameter('annonce_directory').'/'.$ad->getId().'/'.$ad->getImage2());
            }
            if($ad->getImage3()){
                unlink($this->getParameter('annonce_directory').'/'.$ad->getId().'/'.$ad->getImage3());
            }
            $filesystem = new Filesystem();
            $filesystem->remove($this->getParameter('annonce_directory').'/'.$ad->getId());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($ad);
            $entityManager->flush();
        }

        return $this->redirectToRoute('genAds');
    }

}
