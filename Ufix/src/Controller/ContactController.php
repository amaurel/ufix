<?php

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Form\Type\ContactType;
use App\Entity\Contact;
use App\Notification\ContactNotification;

class ContactController extends AbstractController
{
    /**
     * @Route("/contact", name="contact")
     */
    public function showContact(Request $request, ContactNotification $notification): Response
    {
        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $notification->notify($contact);

            $this->addFlash('success', 'Email sent');
            return $this->redirectToRoute('home_page');
        }

        return $this->render('/contact/contact.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
