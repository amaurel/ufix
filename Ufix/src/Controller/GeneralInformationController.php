<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/informations_generales")
 */
class GeneralInformationController extends AbstractController
{
    /**
     * @Route("/", name="general_information")
     */
    public function index()
    {
        return $this->render('general_information/index.html.twig');
    }

    /**
     * @Route("/cgu", name="cgu")
     */
    public function showCGU()
    {
        return $this->render('general_information/cgu.html.twig');
    }

    /**
     * @Route("/mentions_legales", name="legal_mentions")
     */
    public function showLegalMentions()
    {
        return $this->render('general_information/legalMentions.html.twig');
    }

    /**
     * @Route("/equipe", name="equipe")
     */
    public function showTeam()
    {
        return $this->render('general_information/ourTeam.html.twig');
    }

    /**
     * @Route("/informations_generales/ufix", name="ufix")
     */
    public function showUfix()
    {
        return $this->render('general_information/ufix.html.twig');
    }

    /**
     * @Route("/green-it", name="green_it")
     */
    public function showGreenIt()
    {
        return $this->render('general_information/greenIt.html.twig');
    }
}
