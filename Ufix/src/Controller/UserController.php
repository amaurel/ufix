<?php

namespace App\Controller;

use App\Entity\Report;
use App\Entity\User;
use App\Form\Type\ReportType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\Type\ModifyUserType;
use App\Entity\Ad;
use App\Entity\Purchase;
use App\Entity\PurchaseRequest;
use App\Entity\RepairProposition;
use App\Entity\Repair;
use App\Entity\RepairState;
use App\Entity\PurchaseState;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class UserController extends AbstractController
{
  /**
     * @Route("/profil", name="profil_page")
     */
    public function showProfilPage(Request $request, Security $security)
    {
        $user = $security->getUser();
        // dump($user->getFirstName());
        // die;

        $modifyUserForm = $this->createForm(ModifyUserType::class, $user);
        $modifyUserForm->handleRequest($request);

        if ($modifyUserForm->isSubmitted() && $modifyUserForm->isValid()) {
            // dump("ouais");
            // die;
            $em = $this->getDoctrine()->getManager();
            $data = $modifyUserForm->getData();
            $modifiedUser = $security->getUser();
            //    dump($data);
            //    die;
            $image = $modifyUserForm->get('profilePicture')->getData();
            //$image = $data->getProfilePicture();
            
            // this condition is needed because the 'brochure' field is not required
            // so the PDF file must be processed only when a file is uploaded
            if ($image) {
                $newFilename = $user->getId().'.'.$image->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $image->move(
                        $this->getParameter('profil_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $modifiedUser->setProfilePicture($newFilename);
            }



            $modifiedUser->setFirstName($data->getFirstName());
            $modifiedUser->setLastName($data->getLastName());
            $modifiedUser->setAdress($data->getAdress());
            $modifiedUser->setPostCode($data->getPostCode());
            $modifiedUser->setCountry($data->getCountry());
            $modifiedUser->setCity($data->getCity());



            $em->persist($modifiedUser);
            $em->flush();
            $this->addFlash('success', 'Profil actualisé !');

            return $this->redirectToRoute('profil_page');
        }


        return $this->render('/user/profil.html.twig', [
            'user' => $user,
            'modifyUserForm' => $modifyUserForm->createView()
        ]);
    }

    /**
     * @Route("/my-ads", name="my_ads")
     */
    public function showMyAds(Security $security)
    {

        $ads = $security->getUser()->getOwnedAds();
        // dump($security->getUser());
        // die;

        return $this->render('/user/myAds.html.twig', [
            'ads' => $ads
        ]);
    }

    /**
     * @Route("/my-ad/details/{id}", name="my_ad_details")
     */
    public function showMyAdDetails(Security $security, Ad $ad)
    {
        // dump($ad->getAdStates());
        // dump($ad->getCurrentAdState());
        // die;
        // $ads = $security->getUser()->getOwnedAds();
        return $this->render('/user/myAdDetails.html.twig', [
            'ad' => $ad
        ]);
    }

        /**
     * @Route("/my-ad/details/{id}/refuse-repair-proposition", name="refuse_repair_proposition")
     */
    public function refuseRepairProposition(RepairProposition $repairProposition)
    {

        // die;
        $em = $this->getDoctrine()->getManager();
        $ad = $repairProposition->getAd();
        $ad->removeRepairProposition($repairProposition);
        // $repairProposition->setArchived(true);
        // $repairProposition->setAccepted(false);

        
        // dump($ad->getCurrentAdState());
        // die;

        $em->persist($ad);
        $em->flush();

        // dump($ad);
        // die;

        // $ads = $security->getUser()->getOwnedAds();
        return $this->render('/user/myAdDetails.html.twig', [
            'ad' => $ad
        ]);
    }


        /**
     * @Route("/my-ad/details/{id}/accept-repair-proposition", name="accept_repair_proposition")
     */
    public function acceptRepairProposition(RepairProposition $repairProposition)
    {
        $repairStateRepository = $this->getDoctrine()->getRepository(RepairState::class);
        $states = $repairStateRepository->findAll();

        $em = $this->getDoctrine()->getManager();
        $ad = $repairProposition->getAd();
        // $ad->removeRepairProposition($repairProposition);

        $repair = new Repair();
        $repair->setPrice($repairProposition->getPrice());
        $repair->setDescription($repairProposition->getDescription());
        $repair->setRepairer($repairProposition->getProposer());
        $repair->setAd($repairProposition->getAd());
        foreach($states as $s) {
            $repair->AddState($s);
        }
        $repair->nextState();
        
        $ad->setRepair($repair);
        $ad->setVisible(false);

        $repairProposition->getAd()->nextState();
        $repairProposition->setArchived(true);
        $repairProposition->setAccepted(true);

        $em->persist($repair);

        foreach($ad->getRepairPropositions() as $rp) {
            $em->remove($rp);
        }
        $em->flush();

        return $this->render('/user/myAdDetails.html.twig', [
            'ad' => $ad,
        ]);
    }

      /**
     * @Route("/my-ad/details/{id}/pay-repair", name="pay_repair")
     */
    public function payRepair(Repair $repair)
    {

        $em = $this->getDoctrine()->getManager();
        $ad = $repair->getAd();
        
        $repair->nextState();
        $ad->nextState();
    

        $em->persist($repair);
        $em->persist($ad);
        $em->flush();

        return $this->render('/user/myAdDetails.html.twig', [
            'ad' => $ad,
        ]);
    }

    /**
     * @Route("/my-ad/details/{id}/notify-product-sent-to-repairer", name="notify_product_sent_to_repairer")
     */
    public function notifyProductSentToRepairer(Repair $repair)
    {

        $em = $this->getDoctrine()->getManager();
        $ad = $repair->getAd();
        
        $repair->nextState();
        $ad->nextState();
       
        $em->persist($repair);
        $em->persist($ad);
        $em->flush();

        return $this->render('/user/myAdDetails.html.twig', [
            'ad' => $ad,
        ]);
    }

    /**
     * @Route("/my-ad/details/{id}/notify-product-reception-from-owner", name="notify_product_reception_from_owner")
     */
    public function notifyProductReceptionFromOwner(Repair $repair)
    {

        // die;
        $em = $this->getDoctrine()->getManager();
        $ad = $repair->getAd();
        // $ad->removeRepairProposition($repairProposition);
        
        $repair->nextState();
        $ad->nextState();
       
        $em->persist($repair);
        
        $em->persist($ad);
        $em->flush();

        return $this->render('/user/myRepairDetails.html.twig', [
            'repair' => $repair,
        ]);
    }

    /**
     * @Route("/my-ad/details/{id}/notify-product-sent-to-owner", name="notify_product_sent_to_owner")
     */
    public function notifyProductSentToOwner(Repair $repair)
    {

        // die;
        $em = $this->getDoctrine()->getManager();
        $ad = $repair->getAd();
        // $ad->removeRepairProposition($repairProposition);
        
        $repair->nextState();
        $ad->nextState();
       
        $em->persist($repair);
        $em->persist($ad);
        $em->flush();

        return $this->render('/user/myRepairDetails.html.twig', [
            'repair' => $repair,
        ]);
    }

    /**
     * @Route("/my-ad/details/{id}/confirm-repair", name="confirm_repair")
     */
    public function confirmRepair(Repair $repair)
    {

        // die;
        $em = $this->getDoctrine()->getManager();
        $ad = $repair->getAd();
        // $ad->removeRepairProposition($repairProposition);
        
        $repair->nextState();
        $ad->nextState();

        $ad->setArchived(true);
        $repair->setArchived(true);
       
        $em->persist($repair);
        $em->persist($ad);
        $em->flush();

        return $this->render('/user/myAdDetails.html.twig', [
            'ad' => $ad,
        ]);
    }
      
    /**
     * @Route("/my-repair-propositions", name="my_repair_propositions")
     */
    public function showMyRepairPropositions(Security $security)
    {

        $repairPropositions = $security->getUser()->getRepairPropositions();
        return $this->render('/user/myRepairPropositions.html.twig', [
            'repairPropositions' => $repairPropositions
        ]);
    }

    /**
     * @Route("/my-repair-propositions/details/{id}", name="my_repair_proposition_details")
     */
    public function showMyRepairPropositionDetails(Security $security, RepairProposition $repairProposition)
    {
        return $this->render('/user/myRepairPropositionDetails.html.twig', [
            'repairProposition' => $repairProposition
        ]);
    }

    /**
     * @Route("/my-purchase-requests", name="my_purchase_requests")
     */
    public function showMyPurchaseRequests(Security $security)
    {

        $purchaseRequests = $security->getUser()->getPurchaseRequests();
        return $this->render('/user/myPurchaseRequests.html.twig', [
            'purchaseRequests' => $purchaseRequests
        ]);
    }

    /**
     * @Route("/my-purchase-request/details/{id}", name="my_purchase_request_details")
     */
    public function showMyPurchaseRequestDetails(Security $security, PurchaseRequest $purchaseRequest)
    {
        return $this->render('/user/myPurchaseRequestDetails.html.twig', [
            'purchaseRequest' => $purchaseRequest
        ]);
    }


     /**
     * @Route("/my-repairs", name="my_repairs")
     */
    public function showMyRepairs(Security $security)
    {

        $repairs = $security->getUser()->getRepairs();
        return $this->render('/user/myRepairs.html.twig', [
            'repairs' => $repairs
        ]);
    }
    /**
     * @Route("/my-repair/details/{id}", name="my_repair_details")
     */
    public function showMyRepairDetails(Security $security, Repair $repair)
    {
        return $this->render('/user/myRepairDetails.html.twig', [
            'repair' => $repair
        ]);
    }

    /**
     * @Route("/my-purchases", name="my_purchases")
     */
    public function showMyPurchases(Security $security)
    {

        $purchases = $security->getUser()->getPurchases();
        return $this->render('/user/myPurchases.html.twig', [
            'purchases' => $purchases
        ]);
    }

     /**
     * @Route("/my-purchase/details/{id}", name="my_purchase_details")
     */
    public function showMyPurchaseDetails(Security $security, Purchase $purchase)
    {
        // dump($purchase->getCurrentState());
        // die;
        return $this->render('/user/myPurchaseDetails.html.twig', [
            'purchase' => $purchase
        ]);
    }

    

/**
     * @Route("/add-saved-ad/{userId}/{adId}", name="add_saved_ad")
     *
     * @ParamConverter("user", options={"mapping": {"userId" : "id"}})
     * @ParamConverter("ad", options={"mapping": {"adId" : "id"}})
     */
    public function addSavedAd(Security $security, User $user, Ad $ad, Request $request)
    {
        $user->addSavedAd($ad);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();

        $report = new Report();
        $reportForm = $this->createForm(ReportType::class, $report);
        $reportForm->handleRequest($request);

        if ($reportForm->isSubmitted() && $reportForm->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $report->setAds($ad);
            $report->setCreatedAt(new \DateTime('now'));

            $em->persist($report);
            $em->flush();
        }

        $this->addFlash('success', 'Annonce enregistrée !');

        return $this->render('/ad/adDetails.html.twig', [
            'ad' => $ad,
            'reportForm' => $reportForm->createView(),
        ]);
    }

    /**
     * @Route("/users/{userId}/saved-ads", name="my_saved_ads")
     *
     * @ParamConverter("user", options={"mapping": {"userId" : "id"}})
     */
    public function usersSavedAd(User $user)
    {
        $ads = $user->getSavedAds();
        return $this->render('/user/mySavedAds.html.twig', [
            "ads" => $ads,
        ]);
    }

    // /**
    //  * @Route("/{id}", name="user_delete", methods={"DELETE"})
    //  */
    // public function delete(Request $request, User $user): Response
    // {
    //     if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
    //         $entityManager = $this->getDoctrine()->getManager();
    //         $entityManager->remove($user);
    //         $entityManager->flush();
    //     }

    //     return $this->redirectToRoute('genUsers');
    // }



    /**
     * @Route("/my-ad/details/{id}/refuse-purchase-request", name="refuse_purchase_request")
     */
    public function refusePurchaseRequest(PurchaseRequest $purchaseRequest)
    {

        // die;
        $em = $this->getDoctrine()->getManager();
        $ad = $purchaseRequest->getAd();
        $ad->removePurchaseRequest($purchaseRequest);
        // $purchaseRequest->setArchived(true);
        // $purchaseRequest->setAccepted(false);

        // dump($ad->getCurrentAdState());
        // die;

        $em->persist($ad);
        $em->flush();

        // dump($ad);
        // die;

        // $ads = $security->getUser()->getOwnedAds();
        return $this->render('/user/myAdDetails.html.twig', [
            'ad' => $ad
        ]);
    }


        /**
     * @Route("/my-ad/details/{id}/accept-purchase-request", name="accept_purchase_request")
     */
    public function acceptPurchaseRequest(PurchaseRequest $purchaseRequest)
    {
        $purchaseStateRepository = $this->getDoctrine()->getRepository(PurchaseState::class);
        $states = $purchaseStateRepository->findAll();

        $em = $this->getDoctrine()->getManager();
        $ad = $purchaseRequest->getAd();
        // $ad->removeRepairProposition($repairProposition);

        $purchase = new Purchase();
        $purchase->setPrice($purchaseRequest->getAd()->getProductPrice());
        $purchase->setBuyer($purchaseRequest->getBuyer());
        $purchase->setAd($purchaseRequest->getAd());
        foreach($states as $s) {
            $purchase->AddState($s);
        }
        $purchase->nextState();

        $ad->setPurchase($purchase);        
        $ad->setVisible(false);

        $purchaseRequest->getAd()->nextState();
        // $repairProposition->setArchived(true);
        // $repairProposition->setAccepted(true);

        $em->persist($purchase);

        foreach($ad->getPurchaseRequests() as $pr) {
            $em->remove($pr);
        }
        $em->flush();

        return $this->render('/user/myAdDetails.html.twig', [
            'ad' => $ad,
        ]);
    }

      /**
     * @Route("/my-ad/details/{id}/pay-purchase", name="pay_purchase")
     */
    public function payPurchase(Purchase $purchase)
    {

        $em = $this->getDoctrine()->getManager();
        $ad = $purchase->getAd();
        
        $purchase->nextState();
        $ad->nextState();
    
        
        $em->persist($purchase);
        $em->persist($ad);
        $em->flush();

        return $this->render('/user/myPurchaseDetails.html.twig', [
            'purchase' => $purchase,
        ]);
    }

     /**
     * @Route("/my-ad/details/{id}/notify-product-sent-to-buyer", name="notify_product_sent_to_buyer")
     */
    public function notifyProductSentToBuyer(Purchase $purchase)
    {

        $em = $this->getDoctrine()->getManager();
        $ad = $purchase->getAd();
        
        $purchase->nextState();
        $ad->nextState();
       
        $em->persist($purchase);
        $em->persist($ad);
        $em->flush();

        return $this->render('/user/myAdDetails.html.twig', [
            'ad' => $ad,
        ]);
    }
/**
     * @Route("/my-ad/details/{id}/confirm-purchase", name="confirm_purchase")
     */
    public function confirmPurchase(Purchase $purchase)
    {

        // die;
        $em = $this->getDoctrine()->getManager();
        $ad = $purchase->getAd();
        // $ad->removeRepairProposition($repairProposition);
        
        $purchase->nextState();
        $ad->nextState();

        $ad->setArchived(true);
        $purchase->setArchived(true);
        
        $em->persist($purchase);
        $em->persist($ad);
        $em->flush();

        return $this->render('/user/myPurchaseDetails.html.twig', [
            'purchase' => $purchase,
        ]);
    }
    
}
