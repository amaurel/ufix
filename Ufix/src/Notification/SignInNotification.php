<?php

namespace App\Notification;

use App\Entity\User;
use Twig\Environment;

class SignInNotification {

    /**
     * @var \Swift_Mailer
     */
    private $mailer;
    /**
     * @var Environment
     */
    private $renderer;

    public function __construct(\Swift_Mailer $mailer, Environment $renderer)
    {
        $this->mailer = $mailer;
        $this->renderer = $renderer;
    }

    public function notify(User $user){
    $message = (new \Swift_Message( 'Message : ' . $user->getFirstName()))
        ->setFrom('ufixfr@gmail.com')
        ->setTo( $user->getEmail())
        ->setReplyTo( $user->getEmail())
        ->setBody($this->renderer->render('emails/signIn.html.twig', [
            'user' => $user
        ]), 'text/html');
    $this->mailer->send($message);
    }
}
