<?php

namespace App\DataFixtures;

use App\Entity\Device;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\User;
use App\Entity\Ad;
use App\Entity\AdPurpose;
use App\Entity\AdState;
use App\Entity\PurchaseState;
use App\Entity\RepairProposition;
use App\Entity\RepairPropositionState;
use App\Entity\RepairState;
use App\Entity\PurchaseRequestState;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Repository\AdStateRepository;
use Doctrine\ORM\EntityManagerInterface;


class AppFixtures extends Fixture
{

    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);

        // USER 1 [ADMIN]
        $user = new User();
        $user->setEmail("leo@gmail.com");
        $user->setPassword($this->passwordEncoder->encodePassword($user, 'leo'));
        $user->setRoles(['ROLE_ADMIN']);
        $user->setFirstName("leo");
        $user->setLastName("bar");
        $user->setAdress("343 Oak Avenue");
        $user->setPostCode(07601);
        $user->setCountry("France");
        $user->setCity("Bordeaux");
        $manager->persist($user);
        // USER 2
        $user = new User();
        $user->setEmail("kevin@gmail.com");
        $user->setPassword($this->passwordEncoder->encodePassword($user, 'kevin'));
        $user->setRoles(['ROLE_CLASSIC']);
        $user->setFirstName("kevin");
        $user->setLastName("deasington");
        $user->setAdress("43 rue du iop");
        $user->setPostCode(63333);
        $user->setCountry("France");
        $user->setCity("Poliu");
        $manager->persist($user);
        // USER 3
        $user = new User();
        $user->setEmail("lea@gmail.com");
        $user->setPassword($this->passwordEncoder->encodePassword($user, 'lea'));
        $user->setRoles(['ROLE_CLASSIC']);
        $user->setFirstName("lea");
        $user->setLastName("pigo");
        $user->setAdress("21 rue du trypoli");
        $user->setPostCode(56093);
        $user->setCountry("France");
        $user->setCity("Kitten");
        $manager->persist($user);
        // USER 4
        $user = new User();
        $user->setEmail("paul@gmail.com");
        $user->setPassword($this->passwordEncoder->encodePassword($user, 'paul'));
        $user->setRoles(['ROLE_CLASSIC']);
        $user->setFirstName("paul");
        $user->setLastName("bigo");
        $user->setAdress("9 rue du aser");
        $user->setPostCode(12093);
        $user->setCountry("France");
        $user->setCity("Lyon");
        $manager->persist($user);
        // USER 5
        $user = new User();
        $user->setEmail("gael@gmail.com");
        $user->setPassword($this->passwordEncoder->encodePassword($user, 'gael'));
        $user->setRoles(['ROLE_CLASSIC']);
        $user->setFirstName("gael");
        $user->setLastName("zerty");
        $user->setAdress("64 boulevard dupuis");
        $user->setPostCode(42568);
        $user->setCountry("France");
        $user->setCity("Beze");
        $manager->persist($user);

        // DEVICE TYPE 1
        $mobile = new Device();
        $mobile->setType("Mobile");
        $manager->persist($mobile);
        // DEVICE TYPE 2
        $tablette = new Device();
        $tablette->setType("Tablette");
        $manager->persist($tablette);
        // DEVICE TYPE 3
        $ordinateur = new Device();
        $ordinateur->setType("Ordinateur");
        $manager->persist($ordinateur);



        // // REPORT 1
        // $report = new Report();
        // $report->setKeywords("Annonce ambigue");
        // $report->setContent("gggggggggg rrrrrrrrrrr tttttttt gfffffffff");
        // $manager->persist($report);
        // // REPORT 2
        // $report = new Report();
        // $report->setKeywords("Annonce ambigue 2");
        // $report->setContent("hhhhhgggg rrrghghrrrrrrrr tttthttttt gfffffffff");
        // $report->setAds("Annonce ambigue 2");
        // $manager->persist($report);

        // $newAd = new Ad();
        // $newAd->setProductCategory(1);
        // $newAd->setProductName("Honor 7x");
        // $newAd->setProductBreakDescription("lé flingué");
        // $newAd->setProductPrice(83);
        // $newAd->setAdDescription("jlé flingué réparé le plz");
        // $newAd->setAdType(1);
        // $newAd->setOwner($user);

        // $user->addOwnedAd($newAd);
        // $manager->persist($newAd);
        // $manager->persist($user);

        $toRepair = new AdPurpose();
        $toRepair->setType("Faire réparer");
        $manager->persist($toRepair);

        $toSell = new AdPurpose();
        $toSell->setType("Vendre");
        $manager->persist($toSell);

        $adStateToRepair0 = new AdState();
        $adStateToRepair0->setMessage("Annonce déposée, en attente de propositions de réparation");
        $adStateToRepair0->setAdPurpose($toRepair);
        $adStateToRepair0->setTemplate("user/adStatesToRepair/adState0.html.twig");
        $manager->persist($adStateToRepair0);

        $adStateToRepair1 = new AdState();
        $adStateToRepair1->setMessage("Vous avez des propositions de réparation en attente");
        $adStateToRepair1->setAdPurpose($toRepair);
        $adStateToRepair1->setTemplate("user/adStatesToRepair/adState1.html.twig");
        $manager->persist($adStateToRepair1);

        $adStateToRepair2 = new AdState();
        $adStateToRepair2->setMessage("Vous avez accepté une proposition de réparation, veuillez procéder au paiement pour passer à l'étape suivante");
        $adStateToRepair2->setAdPurpose($toRepair);
        $adStateToRepair2->setTemplate("user/adStatesToRepair/adState2.html.twig");
        $manager->persist($adStateToRepair2);

        $adStateToRepair3 = new AdState();
        $adStateToRepair3->setMessage("Paiement enregistré ! Veuillez notifier lorsque vous aurez envoyé le produit au réparateur");
        $adStateToRepair3->setAdPurpose($toRepair);
        $adStateToRepair3->setTemplate("user/adStatesToRepair/adState3.html.twig");
        $manager->persist($adStateToRepair3);

        $adStateToRepair4 = new AdState();
        $adStateToRepair4->setMessage("Vous recevrez une notification lorsque le réparateur aura reçu votre produit");
        $adStateToRepair4->setAdPurpose($toRepair);
        $adStateToRepair4->setTemplate("user/adStatesToRepair/adState4.html.twig");
        $manager->persist($adStateToRepair4);

        $adStateToRepair5 = new AdState();
        $adStateToRepair5->setMessage("Le réparateur a bien reçu votre produit et s'occupe de le réparer !");
        $adStateToRepair5->setAdPurpose($toRepair);
        $adStateToRepair5->setTemplate("user/adStatesToRepair/adState5.html.twig");
        $manager->persist($adStateToRepair5);

        $adStateToRepair6 = new AdState();
        $adStateToRepair6->setMessage("Le réparateur a fini de réparer votre produit et vous l'a envoyé. Veuillez confirmer la réception et la réparation de votre produit");
        $adStateToRepair6->setAdPurpose($toRepair);
        $adStateToRepair6->setTemplate("user/adStatesToRepair/adState6.html.twig");
        $manager->persist($adStateToRepair6);

        $adStateToRepair7 = new AdState();
        $adStateToRepair7->setMessage("Annonce archivée");
        $adStateToRepair7->setAdPurpose($toRepair);
        $adStateToRepair7->setTemplate("user/adStatesToRepair/adState7.html.twig");
        $manager->persist($adStateToRepair7);
        //////////////////////////////////////////

        $adStateToSell0 = new AdState();
        $adStateToSell0->setMessage("Annonce déposée, en attente de demandes d'achat");
        $adStateToSell0->setAdPurpose($toSell);
        $adStateToSell0->setTemplate("user/adStatesToSell/adState0.html.twig");
        $manager->persist($adStateToSell0);

        $adStateToSell1 = new AdState();
        $adStateToSell1->setMessage("Vous avez des demandes d'achat en attente");
        $adStateToSell1->setAdPurpose($toSell);
        $adStateToSell1->setTemplate("user/adStatesToSell/adState1.html.twig");
        $manager->persist($adStateToSell1);

        $adStateToSell2 = new AdState();
        $adStateToSell2->setMessage("Demande acceptée, en attente du paiement de l'acheteur");
        $adStateToSell2->setAdPurpose($toSell);
        $adStateToSell2->setTemplate("user/adStatesToSell/adState2.html.twig");
        $manager->persist($adStateToSell2);

        $adStateToSell3 = new AdState();
        $adStateToSell3->setMessage("Paiement de l'acheteur effectué, veuillez notifier lorsque vous enverrez le produit");
        $adStateToSell3->setAdPurpose($toSell);
        $adStateToSell3->setTemplate("user/adStatesToSell/adState3.html.twig");
        $manager->persist($adStateToSell3);

        $adStateToSell4 = new AdState();
        $adStateToSell4->setMessage("notification d'envoi du produit envoyée, en attente de la confirmation de réception de l'acheteur");
        $adStateToSell4->setAdPurpose($toSell);
        $adStateToSell4->setTemplate("user/adStatesToSell/adState4.html.twig");
        $manager->persist($adStateToSell4);

        $adStateToSell5 = new AdState();
        $adStateToSell5->setMessage("L'acheteur a reçu son produit, annonce archivée.");
        $adStateToSell5->setAdPurpose($toSell);
        $adStateToSell5->setTemplate("user/adStatesToSell/adState5.html.twig");
        $manager->persist($adStateToSell5);


        ///////////////////////////////////////////////


        $purchaseState0 = new PurchaseState();
        $purchaseState0->setMessage("Demande d'achat acceptée par le vendeur, veuillez procéder au paiement");
        $purchaseState0->setTemplate("user/purchaseStates/purchaseState0.html.twig");
        $manager->persist($purchaseState0);

        $purchaseState1 = new PurchaseState();
        $purchaseState1->setMessage("Paiement validé, en attente de la notification d'envoi du produit de la part du vendeur");
        $purchaseState1->setTemplate("user/purchaseStates/purchaseState1.html.twig");
        $manager->persist($purchaseState1);

        $purchaseState2 = new PurchaseState();
        $purchaseState2->setMessage("Le vendeur a envoyé votre produit, veuillez le notifier quand vous l'aurez reçu");
        $purchaseState2->setTemplate("user/purchaseStates/purchaseState2.html.twig");
        $manager->persist($purchaseState2);

        $purchaseState3 = new PurchaseState();
        $purchaseState3->setMessage("Achat validé");
        $purchaseState3->setTemplate("user/purchaseStates/purchaseState3.html.twig");
        $manager->persist($purchaseState3);
//////////////////////////////////////////////////



        $repairPropositionState0 = new RepairPropositionState();
        $repairPropositionState0->setMessage("Proposition déposée, en attente de la réponse du propriétaire");
        $repairPropositionState0->setTemplate("user/repairPropositionStates/repairPropositionState0.html.twig");
        $manager->persist($repairPropositionState0);

        //////////////////////////////////////////////////
        $repairState0 = new RepairState();
        $repairState0->setTemplate("user/repairStates/repairState0.html.twig");
        $manager->persist($repairState0);

        $repairState1 = new RepairState();
        $repairState1->setTemplate("user/repairStates/repairState1.html.twig");
        $manager->persist($repairState1);

        $repairState2 = new RepairState();
        $repairState2->setTemplate("user/repairStates/repairState2.html.twig");
        $manager->persist($repairState2);

        $repairState3 = new RepairState();
        $repairState3->setTemplate("user/repairStates/repairState3.html.twig");
        $manager->persist($repairState3);

        $repairState4 = new RepairState();
        $repairState4->setTemplate("user/repairStates/repairState4.html.twig");
        $manager->persist($repairState4);

        $repairState5 = new RepairState();
        $repairState5->setTemplate("user/repairStates/repairState5.html.twig");
        $manager->persist($repairState5);

/////////////////////////////////////////

        $purchaseRequestState0 = new PurchaseRequestState();
        $purchaseRequestState0->setTemplate("user/purchaseRequestStates/purchaseRequestState0.html.twig");
        $manager->persist($purchaseRequestState0);

        /////////////////////////////////////////

        // AD 1
        //   $ad = new Ad();
        // //   $ad->setAdType(2);
        //   $ad->setProductName("MacBokk Pro retina '13'");
        //   $ad->setProductBreakDescription("Mon macbook est à moitié cassé");
        //   $ad->setProductPrice(490);
        //   $ad->setAdDescription("Mon macbook datant de 1920 fonctionn toujours mais de façon alternative");
        //   $ad->setOwner($user);
        //   $ad->setRepairer($user);
        //   $ad->setDevice($ordinateur);
        //   $ad->setAdPurpose($toRepair);
        //   EntityManager $em = $this->getDoctrine()->getManager();
        //   $adStateRepository = $this->entityManager->getRepository(AdState::class);
        //   $states = $adStateRepository->findByAdPurpose($toRepair);
        //   foreach($states as $s) {
        //     $ad->AddAdState($s);
        //   }
        //   $user->addOwnedAd($ad);
        //   $manager->persist($ad);
        //   $manager->persist($user);
        //   // AD 2
        //   $ad = new Ad();
        //   // $ad->setAdType(2);
        //   $ad->setProductName("Acer probook 3");
        //   $ad->setProductBreakDescription("Ventilation HS");
        //   $ad->setProductPrice(350);
        //   $ad->setAdDescription("Mon macbook datant de 1920 fonctionn toujours mais de façon alternative");
        //   $ad->setOwner($user);
        //   $ad->setRepairer($user);
        //   $ad->setDevice($ordinateur);
        //   $ad->setAdPurpose($toRepair);
        //   $adStateRepository = $this->entityManager->getRepository(AdState::class);
        //   $states = $adStateRepository->findByAdPurpose($toRepair);
        //   foreach($states as $s) {
        //     $ad->AddAdState($s);
        //   }
        //   $user->addOwnedAd($ad);
        //   $manager->persist($ad);
        //   $manager->persist($user);

        // $product = new Product();
        // $product->setCategory(0);
        // $product->setName('Iphaone Plus 13');
        // $product->setbreakState('Ecran rayé + batterie qui coule');
        // $product->setPrice(60);
        // $product->setDescription('Je vous présente mon Iphaone Plus 13 qui est cassé, je le vend car il est cassé et ça coule.');
        // $product->setPurpose(0);
        // $product->setUser($user);
        // $manager->persist($product);

        // $product = new Product();
        // $product->setCategory(0);
        // $product->setName('Samsunf Galaxy s3');
        // $product->setbreakState('Pixel mort sur tout l\'écran');
        // $product->setPrice(5);
        // $product->setDescription('On voit plus rien donc je le vend, commercialement votre');
        // $product->setPurpose(0);
        // $product->setUser($user);
        // $manager->persist($product);

        // $product = new Product();
        // $product->setCategory(0);
        // $product->setName('Ouiqo');
        // $product->setbreakState('Prise jack ne marche plus');
        // $product->setPrice(150);
        // $product->setDescription('Mon Ouiqo est tombé dans le sable en vacances et la prise Jack ne fonctionne plus');
        // $product->setPurpose(1);
        // $product->setUser($user);
        // $manager->persist($product);

        // $product = new Product();
        // $product->setCategory(0);
        // $product->setName('Nokia lumia 520');
        // $product->setbreakState('Trop vieux');
        // $product->setPrice(150);
        // $product->setDescription('Mon telephone est devenu lent sur toutes les applications récentes');
        // $product->setPurpose(1);
        // $product->setUser($user);
        // $manager->persist($product);

        $manager->flush();

        //test
    }
}
