<?php

namespace App\Repository;

use App\Entity\PurchaseState;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PurchaseState|null find($id, $lockMode = null, $lockVersion = null)
 * @method PurchaseState|null findOneBy(array $criteria, array $orderBy = null)
 * @method PurchaseState[]    findAll()
 * @method PurchaseState[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PurchaseStateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PurchaseState::class);
    }

    // /**
    //  * @return PurchaseState[] Returns an array of PurchaseState objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PurchaseState
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
