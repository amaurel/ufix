<?php

namespace App\Repository;

use App\Entity\AdPurpose;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method AdPurpose|null find($id, $lockMode = null, $lockVersion = null)
 * @method AdPurpose|null findOneBy(array $criteria, array $orderBy = null)
 * @method AdPurpose[]    findAll()
 * @method AdPurpose[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdPurposeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AdPurpose::class);
    }

    // /**
    //  * @return AdPurpose[] Returns an array of AdPurpose objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AdPurpose
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
