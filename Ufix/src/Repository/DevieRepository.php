<?php

namespace App\Repository;

use App\Entity\Devie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Devie|null find($id, $lockMode = null, $lockVersion = null)
 * @method Devie|null findOneBy(array $criteria, array $orderBy = null)
 * @method Devie[]    findAll()
 * @method Devie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DevieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Devie::class);
    }

    // /**
    //  * @return Devie[] Returns an array of Devie objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Devie
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
