<?php

namespace App\Repository;

use App\Entity\RepairPropositionState;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method RepairPropositionState|null find($id, $lockMode = null, $lockVersion = null)
 * @method RepairPropositionState|null findOneBy(array $criteria, array $orderBy = null)
 * @method RepairPropositionState[]    findAll()
 * @method RepairPropositionState[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RepairPropositionStateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RepairPropositionState::class);
    }

    // /**
    //  * @return RepairPropositionState[] Returns an array of RepairPropositionState objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RepairPropositionState
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
