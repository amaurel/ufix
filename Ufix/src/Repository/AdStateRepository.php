<?php

namespace App\Repository;

use App\Entity\AdState;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method AdState|null find($id, $lockMode = null, $lockVersion = null)
 * @method AdState|null findOneBy(array $criteria, array $orderBy = null)
 * @method AdState[]    findAll()
//  * @method AdState[]    findByAdPurpose(string $criteria)
 * @method AdState[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdStateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AdState::class);
    }

    /**
     * @return AdState[] Returns an array of AdState objects
     */
    
    public function findByAdPurpose($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.adPurpose = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    

    /*
    public function findOneBySomeField($value): ?AdState
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
