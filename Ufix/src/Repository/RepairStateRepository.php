<?php

namespace App\Repository;

use App\Entity\RepairState;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method RepairState|null find($id, $lockMode = null, $lockVersion = null)
 * @method RepairState|null findOneBy(array $criteria, array $orderBy = null)
 * @method RepairState[]    findAll()
 * @method RepairState[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RepairStateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RepairState::class);
    }

    // /**
    //  * @return RepairState[] Returns an array of RepairState objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RepairState
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
