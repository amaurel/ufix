<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Enum\AdType;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AdRepository")
 */
class Ad
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $adType;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $productName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $productBreakDescription;

    /**
     * @ORM\Column(type="integer")
     */
    private $productPrice;

    /**
     * @ORM\Column(type="text")
     */
    private $adDescription;


    // /**
    //  * @var User
    //  * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="ownedAds" )
    //  */
    // private $owner;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="repairedAds" , cascade={"persist"})
     */
    private $repairer;

    // /**
    //  * @var User
    //  * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="products" , cascade={"persist"})
    //  */
    // private $buyer;

    // /**
    //  * @ORM\Column(type="array")
    //  */
    // private $states;

    // /**
    //  * @ORM\Column(type="integer")
    //  */
    // private $currentState;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RepairProposition", mappedBy="ad", orphanRemoval=true)
     */
    private $repairPropositions;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="ownedAds")
     * @ORM\JoinColumn(nullable=false)
     */
    private $owner;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Repair", mappedBy="ad", cascade={"persist", "remove"})
     */
    private $repair;

    /**
     * @ORM\Column(type="boolean")
     */
    private $visible;

     /** @ORM\ManyToOne(targetEntity="App\Entity\Device", inversedBy="ads")
     * @ORM\JoinColumn(nullable=false)
     */
    private $device;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Report", mappedBy="ads")
     */
    private $reports;

    
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\AdPurpose", inversedBy="ads")
     * @ORM\JoinColumn(nullable=false)
     */
    private $adPurpose;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\AdState", inversedBy="ads")
     */
    private $adStates;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\AdState")
     */
    private $currentAdState;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PurchaseRequest", mappedBy="ad", orphanRemoval=true)
     */
    private $purchaseRequests;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Purchase", mappedBy="ad", cascade={"persist", "remove"})
     */
    private $purchase;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $image1;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $image2;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $image3;

    /**
     * @ORM\Column(type="boolean")
     */
    private $archived;

    function __construct()
    {
        
        $this->visible = true;
        $this->adStates = new ArrayCollection();

        $this->reports = new ArrayCollection();
        $this->purchaseRequests = new ArrayCollection();
        $this->archived = false;
        // $this->adType = AdType::REPAIR_AND_GET_BACK;
        // $this->repairPropositions = new ArrayCollection();
    }


  

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAdType(): ?int
    {
        return $this->AdType;
    }

    public function setAdType($adType): self
    {
        $this->adType = $adType;

        return $this;
    }

    public function getProductName(): ?string
    {
        return $this->productName;
    }

    public function setProductName(string $productName): self
    {
        $this->productName = $productName;

        return $this;
    }

    public function getProductBreakDescription(): ?string
    {
        return $this->productBreakDescription;
    }

    public function setProductBreakDescription(string $productBreakDescription): self
    {
        $this->productBreakDescription = $productBreakDescription;

        return $this;
    }

    public function getProductPrice(): ?int
    {
        return $this->productPrice;
    }

    public function setProductPrice(int $productPrice): self
    {
        $this->productPrice = $productPrice;

        return $this;
    }

    public function getAdDescription(): ?string
    {
        return $this->adDescription;
    }

    public function setAdDescription(string $adDescription): self
    {
        $this->adDescription = $adDescription;

        return $this;
    }



    /**
     * @return User
     */
    public function getRepairer(): User
    {
        return $this->repairer;
    }
    /**
     * @param User $user
     */
    public function setRepairer(User $repairer): void
    {
        $this->repairer = $repairer;
    }

    // public function getCurrentState(): ?int
    // {
    //     return $this->currentState;
    // }

    // public function setCurrentState($currentState): self
    // {
    //     $this->currentState = $currentState;

    //     return $this;
    // }

    /**
     * @return Collection|RepairProposition[]
     */
    public function getRepairPropositions(): Collection
    {
        return $this->repairPropositions;
    }

    public function addRepairProposition(RepairProposition $repairProposition): self
    {
        if (!$this->repairPropositions->contains($repairProposition)) {
            $this->repairPropositions[] = $repairProposition;
            $repairProposition->setAd($this);
        }

        return $this;
    }

    public function removeRepairProposition(RepairProposition $repairProposition): self
    {
        if ($this->repairPropositions->contains($repairProposition)) {
            $this->repairPropositions->removeElement($repairProposition);
            // set the owning side to null (unless already changed)
            if ($repairProposition->getAd() === $this) {
                $repairProposition->setAd(null);
                
            }

            if(count($this->repairPropositions) == 0) {
                $this->previousState();
            }
        }

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getRepair(): ?Repair
    {
        return $this->repair;
    }

    public function setRepair(Repair $repair): self
    {
        $this->repair = $repair;

        // set the owning side of the relation if necessary
        if ($repair->getAd() !== $this) {
            $repair->setAd($this);
        }

        return $this;
    }

    public function getVisible(): ?bool
    {
        return $this->visible;
    }

    public function setVisible(bool $visible): self
    {
        $this->visible = $visible;

        return $this;
    }


    public function getDevice(): ?Device
    {
        return $this->device;
    }

    public function setDevice(?Device $device): self
    {
        $this->device = $device;

        return $this;
    }

    /**
     * @return Collection|Report[]
     */
    public function getReports(): Collection
    {
        return $this->reports;
    }

    public function addReport(Report $report): self
    {
        if (!$this->reports->contains($report)) {
            $this->reports[] = $report;
            $report->setAds($this);
        }
    }

    public function getAdPurpose(): ?AdPurpose
    {
        return $this->adPurpose;
    }

    public function setAdPurpose(?AdPurpose $adPurpose): self
    {
        $this->adPurpose = $adPurpose;

        return $this;
    }

    /**
     * @return Collection|AdState[]
     */
    public function getAdStates(): Collection
    {
        return $this->adStates;
    }

    public function addAdState(AdState $adState): self
    {
        if (!$this->adStates->contains($adState)) {
            $this->adStates[] = $adState;
        }

        return $this;
    }

    public function removeReport(Report $report): self
    {
        if ($this->reports->contains($report)) {
            $this->reports->removeElement($report);
            // set the owning side to null (unless already changed)
            if ($report->getAds() === $this) {
                $report->setAds(null);
            }
        }
    }

    public function removeState(AdState $adState): self
    {
        if ($this->adStates->contains($adState)) {
            $this->adStates->removeElement($adState);
        }

        return $this;
    }


    // public function getImage(): ?string
    // {
    //     return $this->image;
    // }

    // public function setImage(string $image): self
    // {
    //     $this->image = $image;

    //     return $this;
    // }

    public function getCurrentAdState(): ?AdState
    {
        return $this->currentAdState;
    }

    public function setCurrentAdState(?AdState $currentAdState): self
    {
        $this->currentAdState = $currentAdState;

        return $this;
    }

 
    public function nextState() {

        if($this->currentAdState == null) {
            $this->setCurrentAdState($this->adStates[0]);
            return;
        } 

        $next = false;

        foreach($this->adStates as $as) {

            if($next == true) {
                $this->currentAdState = $as;
                // dump($this->currentAdState);
                // die;
                $next = false;
                break;
            }


            if($this->currentAdState == $as) {
                $next = true;
            }

            
        }
    }

    public function previousState() {

        if($this->currentAdState == null) {
            // $this->setCurrentAdState($this->adStates[0]);
            return;
        } 

        // $previous = false;
        $previousState = $this->adStates[0];

        

        foreach($this->adStates as $as) {

            // if($previous == true) {
                // dump($this->currentAdState);
                // die;
                // $previous = false;
                // break;
            // }
            

            if($this->currentAdState == $as) {
                $this->currentAdState = $previousState;
            break;

                // $previous = true;
            }
            $previousState = $as;
            
        }
    }

    /**
     * @return Collection|PurchaseRequest[]
     */
    public function getPurchaseRequests(): Collection
    {
        return $this->purchaseRequests;
    }

    public function addPurchaseRequest(PurchaseRequest $purchaseRequest): self
    {
        if (!$this->purchaseRequests->contains($purchaseRequest)) {
            $this->purchaseRequests[] = $purchaseRequest;
            $purchaseRequest->setAd($this);
        }
        return $this;
    }
    
    public function getImage1(): ?string
    {
        return $this->image1;
    }

    public function setImage1(?string $image1): self
    {
        $this->image1 = $image1;

        return $this;
    }

    public function removePurchaseRequest(PurchaseRequest $purchaseRequest): self
    {
        if ($this->purchaseRequests->contains($purchaseRequest)) {
            $this->purchaseRequests->removeElement($purchaseRequest);
            // set the owning side to null (unless already changed)
            if ($purchaseRequest->getAd() === $this) {
                $purchaseRequest->setAd(null);
            }

            if(count($this->purchaseRequests) == 0) {
                $this->previousState();
            }


        }
        return $this;
    }
    public function getImage2(): ?string
    {
        return $this->image2;
    }

    public function setImage2(?string $image2): self
    {
        $this->image2 = $image2;

        return $this;
    }

    public function getPurchase(): ?Purchase
    {
        return $this->purchase;
    }

    public function setPurchase(Purchase $purchase): self
    {
        $this->purchase = $purchase;

        // set the owning side of the relation if necessary
        if ($purchase->getAd() !== $this) {
            $purchase->setAd($this);
        }
        return $this;
    }
    public function getImage3(): ?string
    {
        return $this->image3;
    }

    public function setImage3(?string $image3): self
    {
        $this->image3 = $image3;

        return $this;
    }

    public function getArchived(): ?bool
    {
        return $this->archived;
    }

    public function setArchived(bool $archived): self
    {
        $this->archived = $archived;

        return $this;
    }
}
