<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AdPurposeRepository")
 */
class AdPurpose
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Ad", mappedBy="adPurpose")
     */
    private $ads;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\AdState", mappedBy="adPurpose")
     */
    private $adStates;

    public function __construct()
    {
        $this->ads = new ArrayCollection();
        $this->states = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Ad[]
     */
    public function getAds(): Collection
    {
        return $this->ads;
    }

    public function addAd(Ad $ad): self
    {
        if (!$this->ads->contains($ad)) {
            $this->ads[] = $ad;
            $ad->setAdPurpose($this);
        }

        return $this;
    }

    public function removeAd(Ad $ad): self
    {
        if ($this->ads->contains($ad)) {
            $this->ads->removeElement($ad);
            // set the owning side to null (unless already changed)
            if ($ad->getAdPurpose() === $this) {
                $ad->setAdPurpose(null);
            }
        }

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|AdState[]
     */
    public function getAdStates(): Collection
    {
        return $this->states;
    }

    public function addAdState(AdState $adState): self
    {
        if (!$this->adStates->contains($adState)) {
            $this->adStates[] = $adState;
            $adState->setAdPurpose($this);
        }

        return $this;
    }

    public function removeAdState(AdState $adState): self
    {
        if ($this->adStates->contains($adState)) {
            $this->states->removeElement($adState);
            // set the owning side to null (unless already changed)
            if ($adState->getAdPurpose() === $this) {
                $adState->setAdPurpose(null);
            }
        }

        return $this;
    }


}
