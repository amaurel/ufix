<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RepairPropositionStateRepository")
 */
class RepairPropositionState
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $message;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\RepairProposition", inversedBy="repairPropositionStates")
     */
    private $repairPropositions;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $template;

    public function __construct()
    {
        $this->repairPropositions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return Collection|RepairProposition[]
     */
    public function getRepairPropositions(): Collection
    {
        return $this->repairPropositions;
    }

    public function addRepairProposition(RepairProposition $repairProposition): self
    {
        if (!$this->repairPropositions->contains($repairProposition)) {
            $this->repairPropositions[] = $repairProposition;
        }

        return $this;
    }

    public function removeRepairProposition(RepairProposition $repairProposition): self
    {
        if ($this->repairPropositions->contains($repairProposition)) {
            $this->repairPropositions->removeElement($repairProposition);
        }

        return $this;
    }

    public function getTemplate(): ?string
    {
        return $this->template;
    }

    public function setTemplate(string $template): self
    {
        $this->template = $template;

        return $this;
    }
}
