<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RepairPropositionRepository")
 */
class RepairProposition
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

  



    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Ad", inversedBy="repairPropositions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ad;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="repairPropositions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $proposer;

    /**
     * @ORM\Column(type="boolean")
     */
    private $archived;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $accepted;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\RepairPropositionState", mappedBy="repairPropositions")
     */
    private $states;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\RepairPropositionState")
     * @ORM\JoinColumn(nullable=false)
     */
    private $currentState;

  


    public function __construct()
    {
        $this->repairs = new ArrayCollection();
        $this->archived = false;
        $this->repairPropositionStates = new ArrayCollection();
        $this->states = new ArrayCollection();

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }


    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }


    public function getAd(): ?Ad
    {
        return $this->ad;
    }

    public function setAd(?Ad $ad): self
    {
        $this->ad = $ad;

        return $this;
    }

    public function getProposer(): ?User
    {
        return $this->proposer;
    }

    public function setProposer(?User $proposer): self
    {
        $this->proposer = $proposer;

        return $this;
    }

    public function getArchived(): ?bool
    {
        return $this->archived;
    }

    public function setArchived(bool $archived): self
    {
        $this->archived = $archived;

        return $this;
    }

    public function getAccepted(): ?bool
    {
        return $this->accepted;
    }

    public function setAccepted(?bool $accepted): self
    {
        $this->accepted = $accepted;

        return $this;
    }

    /**
     * @return Collection|RepairPropositionState[]
     */
    public function getStates(): Collection
    {
        return $this->states;
    }

    public function addState(RepairPropositionState $state): self
    {
        if (!$this->states->contains($state)) {
            $this->states[] = $state;
            $state->addRepairProposition($this);
        }

        return $this;
    }

    public function removeState(RepairPropositionState $state): self
    {
        if ($this->states->contains($state)) {
            $this->states->removeElement($state);
            $state->removeRepairProposition($this);
        }

        return $this;
    }

    public function getCurrentState(): ?RepairPropositionState
    {
        return $this->currentState;
    }

    public function setCurrentState(?RepairPropositionState $currentState): self
    {
        $this->currentState = $currentState;

        return $this;
    }

    public function nextState() {

        if($this->currentState == null) {
            $this->setCurrentState($this->states[0]);
            return;
        } 

        $next = false;

        foreach($this->states as $as) {

            if($next == true) {
                $this->currentState = $as;
                // dump($this->currentAdState);
                // die;
                $next = false;
                break;
            }


            if($this->currentState == $as) {
                $next = true;
            }

            
        }
    }

    public function previousState() {

        if($this->currentState == null) {
            // $this->setCurrentAdState($this->adStates[0]);
            return;
        } 

        // $previous = false;
        $previousState = $this->states[0];

        

        foreach($this->states as $as) {

            // if($previous == true) {
                // dump($this->currentAdState);
                // die;
                // $previous = false;
                // break;
            // }
            

            if($this->currentState == $as) {
                $this->currentState = $previousState;
            break;

                // $previous = true;
            }
            $previousState = $as;
            
        }
    }
 



 
}
