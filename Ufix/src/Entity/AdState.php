<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AdStateRepository")
 */
class AdState
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $message;
 
    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Ad", mappedBy="adStates")
     */
    private $ads;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\AdPurpose", inversedBy="adStates")
     * @ORM\JoinColumn(nullable=false)
     */
    private $adPurpose;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $template;

    public function __construct()
    {
        $this->ads = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return Collection|Ad[]
     */
    public function getAds(): Collection
    {
        return $this->ads;
    }

    public function addAd(Ad $ad): self
    {
        if (!$this->ads->contains($ad)) {
            $this->ads[] = $ad;
            $ad->addState($this);
        }

        return $this;
    }

    public function removeAd(Ad $ad): self
    {
        if ($this->ads->contains($ad)) {
            $this->ads->removeElement($ad);
            $ad->removeState($this);
        }

        return $this;
    }

    public function getAdPurpose(): ?AdPurpose
    {
        return $this->adPurpose;
    }

    public function setAdPurpose(?AdPurpose $adPurpose): self
    {
        $this->adPurpose = $adPurpose;

        return $this;
    }

    public function getTemplate(): ?string
    {
        return $this->template;
    }

    public function setTemplate(string $template): self
    {
        $this->template = $template;

        return $this;
    }
}
