<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PurchaseRepository")
 */
class Purchase
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="purchases")
     * @ORM\JoinColumn(nullable=false)
     */
    private $buyer;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Ad", inversedBy="purchase", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $ad;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\PurchaseState", mappedBy="purchases")
     */
    private $states;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PurchaseState")
     * @ORM\JoinColumn(nullable=false)
     */
    private $currentState;

    /**
     * @ORM\Column(type="boolean")
     */
    private $archived;

    public function __construct()
    {
        $this->states = new ArrayCollection();
        $this->archived = false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBuyer(): ?User
    {
        return $this->buyer;
    }

    public function setBuyer(?User $buyer): self
    {
        $this->buyer = $buyer;

        return $this;
    }

    public function getAd(): ?Ad
    {
        return $this->ad;
    }

    public function setAd(Ad $ad): self
    {
        $this->ad = $ad;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return Collection|PurchaseState[]
     */
    public function getStates(): Collection
    {
        return $this->states;
    }

    public function addState(PurchaseState $state): self
    {
        if (!$this->states->contains($state)) {
            $this->states[] = $state;
            $state->addPurchase($this);
        }

        return $this;
    }

    public function removeState(PurchaseState $state): self
    {
        if ($this->states->contains($state)) {
            $this->states->removeElement($state);
            $state->removePurchase($this);
        }

        return $this;
    }

    public function getCurrentState(): ?PurchaseState
    {
        return $this->currentState;
    }

    public function setCurrentState(?PurchaseState $currentState): self
    {
        $this->currentState = $currentState;

        return $this;
    }

    public function nextState() {

        if($this->currentState == null) {
            $this->setCurrentState($this->states[0]);
            return;
        } 

        $next = false;

        foreach($this->states as $as) {

            if($next == true) {
                $this->currentState = $as;
                // dump($this->currentAdState);
                // die;
                $next = false;
                break;
            }


            if($this->currentState == $as) {
                $next = true;
            }

            
        }
    }

    public function previousState() {

        if($this->currentState == null) {
            // $this->setCurrentAdState($this->adStates[0]);
            return;
        } 

        // $previous = false;
        $previousState = $this->states[0];

        

        foreach($this->states as $as) {

            // if($previous == true) {
                // dump($this->currentAdState);
                // die;
                // $previous = false;
                // break;
            // }
            

            if($this->currentState == $as) {
                $this->currentState = $previousState;
            break;

                // $previous = true;
            }
            $previousState = $as;
            
        }
    }

    public function getArchived(): ?bool
    {
        return $this->archived;
    }

    public function setArchived(bool $archived): self
    {
        $this->archived = $archived;

        return $this;
    }
}
