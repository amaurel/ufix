<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PurchaseRequestRepository")
 */
class PurchaseRequest
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $message;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Ad", inversedBy="purchaseRequests")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ad;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="purchaseRequests")
     * @ORM\JoinColumn(nullable=false)
     */
    private $buyer;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $accepted;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\PurchaseRequestState", mappedBy="purchaseRequests")
     */
    private $states;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PurchaseRequestState")
     * @ORM\JoinColumn(nullable=false)
     */
    private $currentState;

    public function __construct()
    {
        $this->states = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(?string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getAd(): ?Ad
    {
        return $this->ad;
    }

    public function setAd(?Ad $ad): self
    {
        $this->ad = $ad;

        return $this;
    }

    public function getBuyer(): ?User
    {
        return $this->buyer;
    }

    public function setBuyer(?User $buyer): self
    {
        $this->buyer = $buyer;

        return $this;
    }

    public function getAccepted(): ?bool
    {
        return $this->accepted;
    }

    public function setAccepted(?bool $accepted): self
    {
        $this->accepted = $accepted;

        return $this;
    }

    /**
     * @return Collection|PurchaseRequestState[]
     */
    public function getStates(): Collection
    {
        return $this->states;
    }

    public function addState(PurchaseRequestState $state): self
    {
        if (!$this->states->contains($state)) {
            $this->states[] = $state;
            $state->addPurchaseRequest($this);
        }

        return $this;
    }

    public function removeState(PurchaseRequestState $state): self
    {
        if ($this->states->contains($state)) {
            $this->states->removeElement($state);
            $state->removePurchaseRequest($this);

            
        }

        return $this;
    }

    public function getCurrentState(): ?PurchaseRequestState
    {
        return $this->currentState;
    }

    public function setCurrentState(?PurchaseRequestState $currentState): self
    {
        $this->currentState = $currentState;

        return $this;
    }

    public function nextState() {

        if($this->currentState == null) {
            $this->setCurrentState($this->states[0]);
            return;
        } 

        $next = false;

        foreach($this->states as $as) {

            if($next == true) {
                $this->currentState = $as;
                // dump($this->currentAdState);
                // die;
                $next = false;
                break;
            }


            if($this->currentState == $as) {
                $next = true;
            }

            
        }
    }

    public function previousState() {

        if($this->currentState == null) {
            // $this->setCurrentAdState($this->adStates[0]);
            return;
        } 

        // $previous = false;
        $previousState = $this->states[0];

        

        foreach($this->states as $as) {

            // if($previous == true) {
                // dump($this->currentAdState);
                // die;
                // $previous = false;
                // break;
            // }
            

            if($this->currentState == $as) {
                $this->currentState = $previousState;
            break;

                // $previous = true;
            }
            $previousState = $as;
            
        }
    }
}
