<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\NotCompromisedPassword as Safe;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;


    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     * @Assert\NotCompromisedPassword
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastName;

    /**
     * @ORM\Column(type="text")
     */
    private $adress;

    /**
     * @ORM\Column(type="integer")
     */
    private $postCode;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Ad", mappedBy="owner")
     */
    private $ownedAds;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RepairProposition", mappedBy="proposer")
     */
    private $repairPropositions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Repair", mappedBy="repairer")
     */
    private $repairs;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PurchaseRequest", mappedBy="buyer")
     */
    private $purchaseRequests;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Purchase", mappedBy="buyer")
     */
    private $purchases;
    
    private $captchaCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $profilePicture;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Ad")
     */
    private $savedAds;

    public function __construct()
    {
        $this->ownedAds = new ArrayCollection();
        $this->repairPropositions = new ArrayCollection();
        $this->repairs = new ArrayCollection();
        $this->reports = new ArrayCollection();
        $this->purchaseRequests = new ArrayCollection();
        $this->purchases = new ArrayCollection();
        $this->savedAds = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getAdress(): ?string
    {
        return $this->adress;
    }

    public function setAdress(string $adress): self
    {
        $this->adress = $adress;

        return $this;
    }

    public function getPostCode(): ?int
    {
        return $this->postCode;
    }

    public function setPostCode(int $postCode): self
    {
        $this->postCode = $postCode;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCaptchaCode(){
        return $this->captchaCode;
    }

    public function setCaptchaCode($captchaCode){
        $this->captchaCode = $captchaCode;
    }
    /**
     * @return Collection|Ad[]
     */
    public function getOwnedAds(): Collection
    {
        return $this->ownedAds;
    }

    public function addOwnedAd(Ad $ownedAd): self
    {
        if (!$this->ownedAds->contains($ownedAd)) {
            $this->ownedAds[] = $ownedAd;
            $ownedAd->setOwner($this);
        }

        return $this;
    }

    public function removeOwnedAd(Ad $ownedAd): self
    {
        if ($this->ownedAds->contains($ownedAd)) {
            $this->ownedAds->removeElement($ownedAd);
            // set the owning side to null (unless already changed)
            if ($ownedAd->getOwner() === $this) {
                $ownedAd->setOwner(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RepairProposition[]
     */
    public function getRepairPropositions(): Collection
    {
        return $this->repairPropositions;
    }

    public function addRepairProposition(RepairProposition $repairProposition): self
    {
        if (!$this->repairPropositions->contains($repairProposition)) {
            $this->repairPropositions[] = $repairProposition;
            $repairProposition->setProposer($this);
        }

        return $this;
    }

    public function removeRepairProposition(RepairProposition $repairProposition): self
    {
        if ($this->repairPropositions->contains($repairProposition)) {
            $this->repairPropositions->removeElement($repairProposition);
            // set the owning side to null (unless already changed)
            if ($repairProposition->getProposer() === $this) {
                $repairProposition->setProposer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Repair[]
     */
    public function getRepairs(): Collection
    {
        return $this->repairs;
    }

    public function addRepair(Repair $repair): self
    {
        if (!$this->repairs->contains($repair)) {
            $this->repairs[] = $repair;
            $repair->setRepairer($this);
        }

        return $this;
    }

    public function removeRepair(Repair $repair): self
    {
        if ($this->repairs->contains($repair)) {
            $this->repairs->removeElement($repair);
            // set the owning side to null (unless already changed)
            if ($repair->getRepairer() === $this) {
                $repair->setRepairer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PurchaseRequest[]
     */
    public function getPurchaseRequests(): Collection
    {
        return $this->purchaseRequests;
    }

    public function addPurchaseRequest(PurchaseRequest $purchaseRequest): self
    {
        if (!$this->purchaseRequests->contains($purchaseRequest)) {
            $this->purchaseRequests[] = $purchaseRequest;
            $purchaseRequest->setBuyer($this);
        }

        return $this;
    }

    public function removePurchaseRequest(PurchaseRequest $purchaseRequest): self
    {
        if ($this->purchaseRequests->contains($purchaseRequest)) {
            $this->purchaseRequests->removeElement($purchaseRequest);
            // set the owning side to null (unless already changed)
            if ($purchaseRequest->getBuyer() === $this) {
                $purchaseRequest->setBuyer(null);
            }
        }
    }

    public function getProfilePicture(): ?string
    {
        return $this->profilePicture;
    }

    public function setProfilePicture(?string $profilePicture): self
    {
        $this->profilePicture = $profilePicture;
        return $this;
    }

    /**
     * @return Collection|Purchase[]
     */
    public function getPurchases(): Collection
    {
        return $this->purchases;
    }

    public function addPurchase(Purchase $purchase): self
    {
        if (!$this->purchases->contains($purchase)) {
            $this->purchases[] = $purchase;
            $purchase->setBuyer($this);
        }

    }
    
    /**
     * @return Collection|Ad[]
     */
    public function getSavedAds(): Collection
    {
        return $this->savedAds;
    }

    public function addSavedAd(Ad $savedAd): self
    {
        if (!$this->savedAds->contains($savedAd)) {
            $this->savedAds[] = $savedAd;
        }

        return $this;
    }

    public function removePurchase(Purchase $purchase): self
    {
        if ($this->purchases->contains($purchase)) {
            $this->purchases->removeElement($purchase);
            // set the owning side to null (unless already changed)
            if ($purchase->getBuyer() === $this) {
                $purchase->setBuyer(null);
            }
        }
    }
    public function removeSavedAd(Ad $savedAd): self
    {
        if ($this->savedAds->contains($savedAd)) {
            $this->savedAds->removeElement($savedAd);
        }

        return $this;
    } 


}
