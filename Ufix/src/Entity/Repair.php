<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RepairRepository")
 */
class Repair
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="repairs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $repairer;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

 

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Ad", inversedBy="repair", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $ad;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\RepairState", mappedBy="repairs")
     */
    private $states;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\RepairState")
     * @ORM\JoinColumn(nullable=false)
     */
    private $currentState;

    /**
     * @ORM\Column(type="boolean")
     */
    private $archived;





    public function __construct()
    {
        $this->states = new ArrayCollection();
        $this->archived = false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRepairer(): ?User
    {
        return $this->repairer;
    }

    public function setRepairer(?User $repairer): self
    {
        $this->repairer = $repairer;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }



    public function getAd(): ?Ad
    {
        return $this->ad;
    }

    public function setAd(Ad $ad): self
    {
        $this->ad = $ad;

        return $this;
    }

    /**
     * @return Collection|RepairState[]
     */
    public function getStates(): Collection
    {
        return $this->states;
    }

    public function addState(RepairState $state): self
    {
        if (!$this->states->contains($state)) {
            $this->states[] = $state;
            $state->addRepair($this);
        }

        return $this;
    }

    public function removeState(RepairState $state): self
    {
        if ($this->states->contains($state)) {
            $this->states->removeElement($state);
            $state->removeRepair($this);
        }

        return $this;
    }

    public function getCurrentState(): ?RepairState
    {
        return $this->currentState;
    }

    public function setCurrentState(?RepairState $currentState): self
    {
        $this->currentState = $currentState;

        return $this;
    }

    public function nextState() {

        if($this->currentState == null) {
            $this->setCurrentState($this->states[0]);
            return;
        } 

        $next = false;

        foreach($this->states as $as) {

            if($next == true) {
                $this->currentState = $as;
                // dump($this->currentAdState);
                // die;
                $next = false;
                break;
            }


            if($this->currentState == $as) {
                $next = true;
            }

            
        }
    }

    public function previousState() {

        if($this->currentState == null) {
            // $this->setCurrentAdState($this->adStates[0]);
            return;
        } 

        // $previous = false;
        $previousState = $this->states[0];

        

        foreach($this->states as $as) {

            // if($previous == true) {
                // dump($this->currentAdState);
                // die;
                // $previous = false;
                // break;
            // }
            

            if($this->currentState == $as) {
                $this->currentState = $previousState;
            break;

                // $previous = true;
            }
            $previousState = $as;
            
        }
    }

    public function getArchived(): ?bool
    {
        return $this->archived;
    }

    public function setArchived(bool $archived): self
    {
        $this->archived = $archived;

        return $this;
    }
   
}
