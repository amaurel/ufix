<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200130140127 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE repair (id INT AUTO_INCREMENT NOT NULL, repairer_id INT NOT NULL, ad_id INT NOT NULL, current_state_id INT NOT NULL, price INT NOT NULL, description LONGTEXT DEFAULT NULL, archived TINYINT(1) NOT NULL, INDEX IDX_8EE4342147C5DFEE (repairer_id), UNIQUE INDEX UNIQ_8EE434214F34D596 (ad_id), INDEX IDX_8EE4342198A046EB (current_state_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE purchase_request_state (id INT AUTO_INCREMENT NOT NULL, template VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE purchase_request_state_purchase_request (purchase_request_state_id INT NOT NULL, purchase_request_id INT NOT NULL, INDEX IDX_569F550021395CC6 (purchase_request_state_id), INDEX IDX_569F55004E4DEF6F (purchase_request_id), PRIMARY KEY(purchase_request_state_id, purchase_request_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE repair_proposition (id INT AUTO_INCREMENT NOT NULL, ad_id INT NOT NULL, proposer_id INT NOT NULL, current_state_id INT NOT NULL, price INT NOT NULL, description LONGTEXT NOT NULL, archived TINYINT(1) NOT NULL, accepted TINYINT(1) DEFAULT NULL, INDEX IDX_12C9A8724F34D596 (ad_id), INDEX IDX_12C9A872B13FA634 (proposer_id), INDEX IDX_12C9A87298A046EB (current_state_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ad_purpose (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ad (id INT AUTO_INCREMENT NOT NULL, repairer_id INT DEFAULT NULL, owner_id INT NOT NULL, device_id INT NOT NULL, ad_purpose_id INT NOT NULL, current_ad_state_id INT DEFAULT NULL, ad_type INT NOT NULL, product_name VARCHAR(255) NOT NULL, product_break_description VARCHAR(255) NOT NULL, product_price INT NOT NULL, ad_description LONGTEXT NOT NULL, visible TINYINT(1) NOT NULL, image1 VARCHAR(50) DEFAULT NULL, image2 VARCHAR(50) DEFAULT NULL, image3 VARCHAR(50) DEFAULT NULL, archived TINYINT(1) NOT NULL, INDEX IDX_77E0ED5847C5DFEE (repairer_id), INDEX IDX_77E0ED587E3C61F9 (owner_id), INDEX IDX_77E0ED5894A4C7D4 (device_id), INDEX IDX_77E0ED58804B428B (ad_purpose_id), INDEX IDX_77E0ED5851149AC8 (current_ad_state_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ad_ad_state (ad_id INT NOT NULL, ad_state_id INT NOT NULL, INDEX IDX_74EB85AE4F34D596 (ad_id), INDEX IDX_74EB85AEEFC03111 (ad_state_id), PRIMARY KEY(ad_id, ad_state_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ad_state (id INT AUTO_INCREMENT NOT NULL, ad_purpose_id INT NOT NULL, message LONGTEXT NOT NULL, template VARCHAR(255) NOT NULL, INDEX IDX_9EF3887E804B428B (ad_purpose_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE repair_state (id INT AUTO_INCREMENT NOT NULL, template VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE repair_state_repair (repair_state_id INT NOT NULL, repair_id INT NOT NULL, INDEX IDX_CB9CFFBB6C40456A (repair_state_id), INDEX IDX_CB9CFFBB43833CFF (repair_id), PRIMARY KEY(repair_state_id, repair_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE purchase_request (id INT AUTO_INCREMENT NOT NULL, ad_id INT NOT NULL, buyer_id INT NOT NULL, current_state_id INT NOT NULL, message LONGTEXT DEFAULT NULL, accepted TINYINT(1) DEFAULT NULL, INDEX IDX_204D45E64F34D596 (ad_id), INDEX IDX_204D45E66C755722 (buyer_id), INDEX IDX_204D45E698A046EB (current_state_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE repair_proposition_state (id INT AUTO_INCREMENT NOT NULL, message LONGTEXT NOT NULL, template VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE repair_proposition_state_repair_proposition (repair_proposition_state_id INT NOT NULL, repair_proposition_id INT NOT NULL, INDEX IDX_A2B7721A9B7E1703 (repair_proposition_state_id), INDEX IDX_A2B7721AB55235E9 (repair_proposition_id), PRIMARY KEY(repair_proposition_state_id, repair_proposition_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE purchase (id INT AUTO_INCREMENT NOT NULL, buyer_id INT NOT NULL, ad_id INT NOT NULL, current_state_id INT NOT NULL, price INT NOT NULL, archived TINYINT(1) NOT NULL, INDEX IDX_6117D13B6C755722 (buyer_id), UNIQUE INDEX UNIQ_6117D13B4F34D596 (ad_id), INDEX IDX_6117D13B98A046EB (current_state_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE report (id INT AUTO_INCREMENT NOT NULL, ads_id INT NOT NULL, content LONGTEXT NOT NULL, created_at DATETIME NOT NULL, keywords LONGTEXT NOT NULL, INDEX IDX_C42F7784FE52BF81 (ads_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, adress LONGTEXT NOT NULL, post_code INT NOT NULL, country VARCHAR(255) NOT NULL, city VARCHAR(255) NOT NULL, profile_picture VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_ad (user_id INT NOT NULL, ad_id INT NOT NULL, INDEX IDX_6FB7599DA76ED395 (user_id), INDEX IDX_6FB7599D4F34D596 (ad_id), PRIMARY KEY(user_id, ad_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE device (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(30) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE purchase_state (id INT AUTO_INCREMENT NOT NULL, message LONGTEXT NOT NULL, template VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE purchase_state_purchase (purchase_state_id INT NOT NULL, purchase_id INT NOT NULL, INDEX IDX_D33AA8D02C1A2F93 (purchase_state_id), INDEX IDX_D33AA8D0558FBEB9 (purchase_id), PRIMARY KEY(purchase_state_id, purchase_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE repair ADD CONSTRAINT FK_8EE4342147C5DFEE FOREIGN KEY (repairer_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE repair ADD CONSTRAINT FK_8EE434214F34D596 FOREIGN KEY (ad_id) REFERENCES ad (id)');
        $this->addSql('ALTER TABLE repair ADD CONSTRAINT FK_8EE4342198A046EB FOREIGN KEY (current_state_id) REFERENCES repair_state (id)');
        $this->addSql('ALTER TABLE purchase_request_state_purchase_request ADD CONSTRAINT FK_569F550021395CC6 FOREIGN KEY (purchase_request_state_id) REFERENCES purchase_request_state (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE purchase_request_state_purchase_request ADD CONSTRAINT FK_569F55004E4DEF6F FOREIGN KEY (purchase_request_id) REFERENCES purchase_request (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE repair_proposition ADD CONSTRAINT FK_12C9A8724F34D596 FOREIGN KEY (ad_id) REFERENCES ad (id)');
        $this->addSql('ALTER TABLE repair_proposition ADD CONSTRAINT FK_12C9A872B13FA634 FOREIGN KEY (proposer_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE repair_proposition ADD CONSTRAINT FK_12C9A87298A046EB FOREIGN KEY (current_state_id) REFERENCES repair_proposition_state (id)');
        $this->addSql('ALTER TABLE ad ADD CONSTRAINT FK_77E0ED5847C5DFEE FOREIGN KEY (repairer_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE ad ADD CONSTRAINT FK_77E0ED587E3C61F9 FOREIGN KEY (owner_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE ad ADD CONSTRAINT FK_77E0ED5894A4C7D4 FOREIGN KEY (device_id) REFERENCES device (id)');
        $this->addSql('ALTER TABLE ad ADD CONSTRAINT FK_77E0ED58804B428B FOREIGN KEY (ad_purpose_id) REFERENCES ad_purpose (id)');
        $this->addSql('ALTER TABLE ad ADD CONSTRAINT FK_77E0ED5851149AC8 FOREIGN KEY (current_ad_state_id) REFERENCES ad_state (id)');
        $this->addSql('ALTER TABLE ad_ad_state ADD CONSTRAINT FK_74EB85AE4F34D596 FOREIGN KEY (ad_id) REFERENCES ad (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ad_ad_state ADD CONSTRAINT FK_74EB85AEEFC03111 FOREIGN KEY (ad_state_id) REFERENCES ad_state (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ad_state ADD CONSTRAINT FK_9EF3887E804B428B FOREIGN KEY (ad_purpose_id) REFERENCES ad_purpose (id)');
        $this->addSql('ALTER TABLE repair_state_repair ADD CONSTRAINT FK_CB9CFFBB6C40456A FOREIGN KEY (repair_state_id) REFERENCES repair_state (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE repair_state_repair ADD CONSTRAINT FK_CB9CFFBB43833CFF FOREIGN KEY (repair_id) REFERENCES repair (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE purchase_request ADD CONSTRAINT FK_204D45E64F34D596 FOREIGN KEY (ad_id) REFERENCES ad (id)');
        $this->addSql('ALTER TABLE purchase_request ADD CONSTRAINT FK_204D45E66C755722 FOREIGN KEY (buyer_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE purchase_request ADD CONSTRAINT FK_204D45E698A046EB FOREIGN KEY (current_state_id) REFERENCES purchase_request_state (id)');
        $this->addSql('ALTER TABLE repair_proposition_state_repair_proposition ADD CONSTRAINT FK_A2B7721A9B7E1703 FOREIGN KEY (repair_proposition_state_id) REFERENCES repair_proposition_state (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE repair_proposition_state_repair_proposition ADD CONSTRAINT FK_A2B7721AB55235E9 FOREIGN KEY (repair_proposition_id) REFERENCES repair_proposition (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE purchase ADD CONSTRAINT FK_6117D13B6C755722 FOREIGN KEY (buyer_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE purchase ADD CONSTRAINT FK_6117D13B4F34D596 FOREIGN KEY (ad_id) REFERENCES ad (id)');
        $this->addSql('ALTER TABLE purchase ADD CONSTRAINT FK_6117D13B98A046EB FOREIGN KEY (current_state_id) REFERENCES purchase_state (id)');
        $this->addSql('ALTER TABLE report ADD CONSTRAINT FK_C42F7784FE52BF81 FOREIGN KEY (ads_id) REFERENCES ad (id)');
        $this->addSql('ALTER TABLE user_ad ADD CONSTRAINT FK_6FB7599DA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_ad ADD CONSTRAINT FK_6FB7599D4F34D596 FOREIGN KEY (ad_id) REFERENCES ad (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE purchase_state_purchase ADD CONSTRAINT FK_D33AA8D02C1A2F93 FOREIGN KEY (purchase_state_id) REFERENCES purchase_state (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE purchase_state_purchase ADD CONSTRAINT FK_D33AA8D0558FBEB9 FOREIGN KEY (purchase_id) REFERENCES purchase (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE repair_state_repair DROP FOREIGN KEY FK_CB9CFFBB43833CFF');
        $this->addSql('ALTER TABLE purchase_request_state_purchase_request DROP FOREIGN KEY FK_569F550021395CC6');
        $this->addSql('ALTER TABLE purchase_request DROP FOREIGN KEY FK_204D45E698A046EB');
        $this->addSql('ALTER TABLE repair_proposition_state_repair_proposition DROP FOREIGN KEY FK_A2B7721AB55235E9');
        $this->addSql('ALTER TABLE ad DROP FOREIGN KEY FK_77E0ED58804B428B');
        $this->addSql('ALTER TABLE ad_state DROP FOREIGN KEY FK_9EF3887E804B428B');
        $this->addSql('ALTER TABLE repair DROP FOREIGN KEY FK_8EE434214F34D596');
        $this->addSql('ALTER TABLE repair_proposition DROP FOREIGN KEY FK_12C9A8724F34D596');
        $this->addSql('ALTER TABLE ad_ad_state DROP FOREIGN KEY FK_74EB85AE4F34D596');
        $this->addSql('ALTER TABLE purchase_request DROP FOREIGN KEY FK_204D45E64F34D596');
        $this->addSql('ALTER TABLE purchase DROP FOREIGN KEY FK_6117D13B4F34D596');
        $this->addSql('ALTER TABLE report DROP FOREIGN KEY FK_C42F7784FE52BF81');
        $this->addSql('ALTER TABLE user_ad DROP FOREIGN KEY FK_6FB7599D4F34D596');
        $this->addSql('ALTER TABLE ad DROP FOREIGN KEY FK_77E0ED5851149AC8');
        $this->addSql('ALTER TABLE ad_ad_state DROP FOREIGN KEY FK_74EB85AEEFC03111');
        $this->addSql('ALTER TABLE repair DROP FOREIGN KEY FK_8EE4342198A046EB');
        $this->addSql('ALTER TABLE repair_state_repair DROP FOREIGN KEY FK_CB9CFFBB6C40456A');
        $this->addSql('ALTER TABLE purchase_request_state_purchase_request DROP FOREIGN KEY FK_569F55004E4DEF6F');
        $this->addSql('ALTER TABLE repair_proposition DROP FOREIGN KEY FK_12C9A87298A046EB');
        $this->addSql('ALTER TABLE repair_proposition_state_repair_proposition DROP FOREIGN KEY FK_A2B7721A9B7E1703');
        $this->addSql('ALTER TABLE purchase_state_purchase DROP FOREIGN KEY FK_D33AA8D0558FBEB9');
        $this->addSql('ALTER TABLE repair DROP FOREIGN KEY FK_8EE4342147C5DFEE');
        $this->addSql('ALTER TABLE repair_proposition DROP FOREIGN KEY FK_12C9A872B13FA634');
        $this->addSql('ALTER TABLE ad DROP FOREIGN KEY FK_77E0ED5847C5DFEE');
        $this->addSql('ALTER TABLE ad DROP FOREIGN KEY FK_77E0ED587E3C61F9');
        $this->addSql('ALTER TABLE purchase_request DROP FOREIGN KEY FK_204D45E66C755722');
        $this->addSql('ALTER TABLE purchase DROP FOREIGN KEY FK_6117D13B6C755722');
        $this->addSql('ALTER TABLE user_ad DROP FOREIGN KEY FK_6FB7599DA76ED395');
        $this->addSql('ALTER TABLE ad DROP FOREIGN KEY FK_77E0ED5894A4C7D4');
        $this->addSql('ALTER TABLE purchase DROP FOREIGN KEY FK_6117D13B98A046EB');
        $this->addSql('ALTER TABLE purchase_state_purchase DROP FOREIGN KEY FK_D33AA8D02C1A2F93');
        $this->addSql('DROP TABLE repair');
        $this->addSql('DROP TABLE purchase_request_state');
        $this->addSql('DROP TABLE purchase_request_state_purchase_request');
        $this->addSql('DROP TABLE repair_proposition');
        $this->addSql('DROP TABLE ad_purpose');
        $this->addSql('DROP TABLE ad');
        $this->addSql('DROP TABLE ad_ad_state');
        $this->addSql('DROP TABLE ad_state');
        $this->addSql('DROP TABLE repair_state');
        $this->addSql('DROP TABLE repair_state_repair');
        $this->addSql('DROP TABLE purchase_request');
        $this->addSql('DROP TABLE repair_proposition_state');
        $this->addSql('DROP TABLE repair_proposition_state_repair_proposition');
        $this->addSql('DROP TABLE purchase');
        $this->addSql('DROP TABLE report');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_ad');
        $this->addSql('DROP TABLE device');
        $this->addSql('DROP TABLE purchase_state');
        $this->addSql('DROP TABLE purchase_state_purchase');
    }
}
