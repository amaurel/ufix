<?php

namespace App\Form\Type;

use App\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


// Import the Captcha Field Type and Validator
use Captcha\Bundle\CaptchaBundle\Form\Type\CaptchaType;
use Captcha\Bundle\CaptchaBundle\Validator\Constraints\ValidCaptcha;

class ContactType extends AbstractType
{

        public function buildForm(FormBuilderInterface $builder, array $options)
        {
            $builder
                ->add('name', TextType::class)
                ->add('email', EmailType::class)
                ->add('message', TextareaType::class)
                // ->add('captchaCode', CaptchaType::class, array(
                //     'captchaConfig' => 'CaptchaContact',
                //     'label' => ' ',
                //     'constraints' => [
                //         new ValidCaptcha([
                //             'message' => 'captcha invalide, réessayez s\'il vous plaît',
                //         ]),
                //     ],
                // ))
            ;
        }

        public function configureOptions(OptionsResolver $resolver)
        {
            $resolver->setDefaults([
                'data_class' => Contact::class
            ]);
        }

}