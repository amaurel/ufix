<?php


namespace App\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use App\Entity\User;





use Symfony\Component\Form\FormBuilderInterface;

class ModifyUserType extends AbstractType
{

        public function buildForm(FormBuilderInterface $builder, array $options)
        {
            // dump($options['data']);
            // die;
            $builder

                ->add('profilePicture', FileType::class, [
                    'label' => 'Image de profil (jpg ou png)',

                    // unmapped means that this field is not associated to any entity property
                    'mapped' => false,
                    'required' => false,

                    // unmapped fields can't define their validation using annotations
                    // in the associated entity, so you can use the PHP constraint classes
                    'constraints' => [
                        new File([
                            'maxSize' => '1024k',
                            'mimeTypes' => [
                                'image/png',
                                'image/jpeg',
                                'image/jpg',
                            ],
                            'mimeTypesMessage' => 'Please upload a valid image',
                        ])
                    ]
                ])

                ->add('firstName', TextType::class, [
                    'label'    => "Prénom",
                    'attr'     => [
                        'current'  => $options['data']->getFirstName()
                    ]
                    

                ])
                ->add('lastName', TextType::class, [
                    'label'    => "Nom",
                    'attr'     => [
                        'current'  => $options['data']->getLastName()
                    ]
                    
                ])
                ->add('adress', TextType::class, [
                    'label'    => "Adresse",
                    'attr'     => [
                        'current'  => $options['data']->getAdress()
                    ]
                    

                ])
                ->add('postCode', NumberType::class, [
                    'label'    => "Code postal",
                    'attr'     => [
                        'current'  => $options['data']->getPostCode()
                    ]
                    
                ])
                ->add('country', TextType::class, [
                    'label'    => "Pays",
                    'attr'     => [
                        'current'  => $options['data']->getCountry()
                    ]
                    
                ])
                ->add('city', TextType::class, [
                    'label'    => "Ville",
                    'attr'     => [
                        'current'  => $options['data']->getCity()
                    ]
                    
                ])
                ->add('submit', SubmitType::class)
            ;
        }

}