<?php

namespace App\Form\Type;

use App\Entity\Report;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // ->add('reportType', ChoiceType::class, [
            //     'choices' => [
            //         'Signalement' => [
            //             'Annonce' => 'ads',
            //             'User' => 'users',
            //         ],
            //     ],
            // ])
            ->add('keywords', TextType::class, [
                'label'    => "Mots clés",
            ])
            ->add('content', TextareaType::class, [
                'label'    => "Raison",
            ])
            ->add('submit', SubmitType::class);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Report::class,
        ]);
    }
}