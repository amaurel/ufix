<?php


namespace App\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;


// Import the Captcha Field Type and Validator
use Captcha\Bundle\CaptchaBundle\Form\Type\CaptchaType;
use Captcha\Bundle\CaptchaBundle\Validator\Constraints\ValidCaptcha;



use Symfony\Component\Form\FormBuilderInterface;

class NewUserType extends AbstractType
{

        public function buildForm(FormBuilderInterface $builder, array $options)
        {
            $builder
                ->add('firstName', TextType::class, [
                    'label'    => "Prénom",
                ])
                ->add('lastName', TextType::class, [
                    'label'    => "Nom",
                ])
                ->add('email', EmailType::class, [
                    'label'    => "Email",
                ])
                ->add('password', RepeatedType::class, [
                    'type' => PasswordType::class,
                    'invalid_message' => 'Les mots de passes ne correspondent pas.',
                    'options' => ['attr' => ['class' => 'password-field']],
                    'required' => true,
                    'first_options'  => ['label' => 'Mot de passe'],
                    'second_options' => ['label' => 'Confirmer le mot de passe'],
                ])
                ->add('adress', TextType::class, [
                    'label'    => "Adresse",
                ])
                ->add('postCode', NumberType::class, [
                    'label'    => "Code postal",
                ])
                ->add('country', TextType::class, [
                    'label'    => "Pays",
                ])
                ->add('city', TextType::class, [
                    'label'    => "Ville",
                ])
                ->add('agree', CheckboxType::class, [
                    'label'    => "I agree to the terms and conditions",
                    'required' => true,
                ])
                ->add('captchaCodeUser', CaptchaType::class, array(
                    'captchaConfig' => 'CaptchaUserRegistration',
                    'constraints' => [
                        new ValidCaptcha([
                            'message' => 'captcha invalide, réessayez s\'il vous plaît',
                        ]),
                    ],
                ))
                ->add('submit', SubmitType::class)
            ;
        }

}