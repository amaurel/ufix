<?php


namespace App\Form\Type;


use App\Entity\Device;
use App\Entity\AdPurpose;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;

use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Form\Extension\Core\Type\FileType;




use Symfony\Component\Form\FormBuilderInterface;

class NewAdType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('device', EntityType::class, [
                'label' => 'Catégorie',
                'class' => Device::class,
                'choice_label' => 'type',
                'placeholder' => 'Catégorie',

            ])
            ->add('name', TextType::class, [
                'label'    => "Nom du produit",
            ])
            // ->add('image', FileType::class, [
            //     'label' => "Image",
            // ])
            ->add('breakState', TextType::class, [
                'label'    => "Etat du produit",
            ])
            ->add('purpose', EntityType::class, [
                'label' => 'Catégorie',
                'class' => AdPurpose::class,
                'choice_label' => 'type',
                'expanded' => true,
                'multiple' => false,
                'required' => true,
                // 'placeholder' => 'Catégorie',

            ])
            ->add('description', TextareaType::class, [
                'label'    => "Description du produit",
            ])
            ->add('price', NumberType::class, [
                'label'    => "Prix",
            ])
            ->add('image1', FileType::class, [
                'label' => 'Image du produit (jpg ou png)',

                // unmapped means that this field is not associated to any entity property
                'mapped' => false,
                'required' => false,
                // unmapped fields can't define their validation using annotations
                // in the associated entity, so you can use the PHP constraint classes
                'constraints' => [
                    new File([
                        'maxSize' => '1024K',
                        'mimeTypes' => [
                            'image/png',
                            'image/jpeg',
                            'image/jpg',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid image',
                    ])
                ]
            ])
            ->add('image2', FileType::class, [
                'label' => 'Image du produit (jpg ou png)',

                // unmapped means that this field is not associated to any entity property
                'mapped' => false,
                'required' => false,

                // unmapped fields can't define their validation using annotations
                // in the associated entity, so you can use the PHP constraint classes
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'image/png',
                            'image/jpeg',
                            'image/jpg',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid image',
                    ])
                ]
            ])
            ->add('image3', FileType::class, [
                'label' => 'Image du produit (jpg ou png)',

                // unmapped means that this field is not associated to any entity property
                'mapped' => false,
                'required' => false,

                // unmapped fields can't define their validation using annotations
                // in the associated entity, so you can use the PHP constraint classes
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'image/png',
                            'image/jpeg',
                            'image/jpg',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid image',
                    ])
                ]
            ])
            ->add('submit', SubmitType::class);

            $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
                $product = $event->getData();
                $form = $event->getForm();
        
                // checks if the Product object is "new"
                // If no data is passed to the form, the data is "null".
                // This should be considered a new "Product"
                if (!$product || null === $product->getId()) {
                    $form->add('name', TextType::class);
                }
            });
    }

    
}
