/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
// require('../scss/app.scss');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
// const $ = require('jquery');

require("@fortawesome/fontawesome-free/js/all");
require("@fortawesome/fontawesome-free/js/fontawesome");
require("@fortawesome/fontawesome-free/js/solid");
require("@fortawesome/fontawesome-free/js/regular");
require("@fortawesome/fontawesome-free/js/brands");

// menu hamburger
document.addEventListener('DOMContentLoaded', () => {

    const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

    if ($navbarBurgers.length > 0) {

        $navbarBurgers.forEach(el => {
            el.addEventListener('click', () => {

                const target = el.dataset.target;
                const $target = document.getElementById(target);

                el.classList.toggle('is-active');
                $target.classList.toggle('is-active');

            });
        });
    }
});

// // Modal
// var btn = document.querySelector('#showModal');
// var modalDlg = document.querySelector('#myModal');
// var imageModalCloseBtn = document.querySelector('#closeModal');
// btn.addEventListener('click', function(){
//   modalDlg.classList.add('is-active');
// });

// imageModalCloseBtn.addEventListener('click', function(){
//   modalDlg.classList.remove('is-active');
// });

