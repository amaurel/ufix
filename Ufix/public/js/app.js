document.addEventListener("DOMContentLoaded", function() {
  var $navbarBurgers = Array.prototype.slice.call(
    document.querySelectorAll(".navbar-burger"),
    0
  );

  if ($navbarBurgers.length > 0) {
    $navbarBurgers.forEach(function(el) {
      el.addEventListener("click", function() {
        var target = el.dataset.target;
        var $target = document.getElementById(target);
        el.classList.toggle("is-active");
        $target.classList.toggle("is-active");
      });
    });
  }
});

document.addEventListener("DOMContentLoaded", function() {
  (document.querySelectorAll(".notification .delete") || []).forEach(function(
    $delete
  ) {
    $notification = $delete.parentNode;
    $delete.addEventListener("click", function() {
      $notification.parentNode.removeChild($notification);
    });
  });
});

$(document).ready(function() {
  // au clic sur un lien
  $("a.scroll").on("click", function(evt) {
    // bloquer le comportement par défaut: on ne rechargera pas la page
    evt.preventDefault(); // enregistre la valeur de l'attribut  href dans la variable target

    var target = $(this).attr("href");
    /* le sélecteur $(html, body) permet de corriger un bug sur chrome 
    et safari (webkit) */

    $("html, body") // on arrête toutes les animations en cours
      .stop()
      /* on fait maintenant l'animation vers le haut (scrollTop) vers 
     notre ancre target */
      .animate(
        {
          scrollTop: $(target).offset().top
        },
        1000
      );
  });
});

//Lorsque l'on arrive sur la page messaging, les messages les plus récents sont affichés (scrollbar vers le bas)
$(document).ready(function() {
  $("#messages")
    .parent()
    .scrollTop(
      $("#messages")
        .parent()
        .prop("scrollHeight")
    );
});
//Fonction d'envoie de message dans la page messaging
function sendMessage() {
  var valueMessage = $("#message-to-send").val();
  var dt = new Date();
  var time = dt.getHours() + ":" + dt.getMinutes();
  if (valueMessage.length != 0 && !isNullOrEmpty(valueMessage)) {
    $("#messages").append(
      '<li class="clearfix"><div class="message-data has-text-right"><span class="message-data-time">' +
        time +
        ', Today</span>&nbsp; &nbsp;<span class="message-data-name">Olia</span></div><div class="message other-message is-pulled-right">' +
        valueMessage +
        "</div></li>"
    );
    $("#message-to-send").val("");
    $("#messages")
      .parent()
      .scrollTop(
        $("#messages")
          .parent()
          .prop("scrollHeight")
      );
  }
}
//Fonction pour vérifier que le message ne contient pas uniquement des espaces.
function isNullOrEmpty(str) {
  return !str || !str.trim();
}

function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $("#imagePreview").css(
        "background-image",
        "url(" + e.target.result + ")"
      );
      $("#imagePreview").hide();
      $("#imagePreview").fadeIn(650);
    };
    reader.readAsDataURL(input.files[0]);
  }
}
$("#imageUpload").change(function() {
  readURL(this);
});

function readURL2(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e2) {
      $("#imagePreview2").css(
        "background-image",
        "url(" + e2.target.result + ")"
      );
      $("#imagePreview2").hide();
      $("#imagePreview2").fadeIn(650);
    };
    reader.readAsDataURL(input.files[0]);
  }
}
$("#imageUpload2").change(function() {
  readURL2(this);
});

function readURL3(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e3) {
      $("#imagePreview3").css(
        "background-image",
        "url(" + e3.target.result + ")"
      );
      $("#imagePreview3").hide();
      $("#imagePreview3").fadeIn(650);
    };
    reader.readAsDataURL(input.files[0]);
  }
}
$("#imageUpload3").change(function() {
  readURL3(this);
});

function isSend() {
  var selectNotif = document.getElementById("notif");
  selectNotif.classList.remove("is-hidden");
}
function isDelete1() {
  var selectAds = document.getElementById("ads_delete_1");
  selectAds.classList.add("is-hidden");
}
function isDelete2() {
  var selectAds = document.getElementById("ads_delete_2");
  selectAds.classList.add("is-hidden");
}
function isDelete3() {
  var selectAds = document.getElementById("ads_delete_3");
  selectAds.classList.add("is-hidden");
}
function isDelete4() {
  var selectAds = document.getElementById("ads_delete_4");
  selectAds.classList.add("is-hidden");
}

// --------------------- SLIDER
var sliderImage = $(".slider__images-image");

sliderImage.each(function(index) {
  $(".js__slider__pagers").append("<li>" + (index + 1) + "</li>");
});

// set up vars
var sliderPagers = "ol.js__slider__pagers li",
  sliderPagersActive = ".js__slider__pagers li.active",
  sliderImages = ".js__slider__images",
  sliderImagesItem = ".slider__images-item",
  sliderControlNext = ".slider__control--next",
  sliderControlPrev = ".slider__control--prev",
  sliderSpeed = 5000,
  lastElem = $(sliderPagers).length - 1,
  sliderTarget;

// add css heigt to slider images list
function checkImageHeight() {
  var sliderHeight = $(".slider__images-image:visible").height();
  $(sliderImages).css("height", sliderHeight + "px");
}

sliderImage.on("load", function() {
  checkImageHeight();
  $(sliderImages).addClass("loaded");
});
$(window).resize(function() {
  checkImageHeight();
});

// set up first slide
$(sliderPagers)
  .first()
  .addClass("active");
$(sliderImagesItem)
  .hide()
  .first()
  .show();

// transition function
function sliderResponse(sliderTarget) {
  $(sliderImagesItem)
    .fadeOut(300)
    .eq(sliderTarget)
    .fadeIn(300);
  $(sliderPagers)
    .removeClass("active")
    .eq(sliderTarget)
    .addClass("active");
}

// pager controls
$(sliderPagers).on("click", function() {
  if (!$(this).hasClass("active")) {
    sliderTarget = $(this).index();
    sliderResponse(sliderTarget);
    resetTiming();
  }
});

// next/prev controls
$(sliderControlNext).on("click", function() {
  sliderTarget = $(sliderPagersActive).index();
  sliderTarget === lastElem
    ? (sliderTarget = 0)
    : (sliderTarget = sliderTarget + 1);
  sliderResponse(sliderTarget);
  resetTiming();
});
$(sliderControlPrev).on("click", function() {
  sliderTarget = $(sliderPagersActive).index();
  lastElem = $(sliderPagers).length - 1;
  sliderTarget === 0
    ? (sliderTarget = lastElem)
    : (sliderTarget = sliderTarget - 1);
  sliderResponse(sliderTarget);
  resetTiming();
});

// slider timing
function sliderTiming() {
  sliderTarget = $(sliderPagersActive).index();
  sliderTarget === lastElem
    ? (sliderTarget = 0)
    : (sliderTarget = sliderTarget + 1);
  sliderResponse(sliderTarget);
}

// slider autoplay
var timingRun = setInterval(function() {
  sliderTiming();
}, sliderSpeed);

function resetTiming() {
  clearInterval(timingRun);
  timingRun = setInterval(function() {
    sliderTiming();
  }, sliderSpeed);
}

//Permet de supprimer un texte en dessous du captcha
$(function() {
  $("a[title ~= 'BotDetect']").removeAttr("style");
  $("a[title ~= 'BotDetect']").removeAttr("href");
  $("a[title ~= 'BotDetect']").css("visibility", "hidden");
});

//Modal
class BulmaModal {
  constructor(selector) {
    this.elem = document.querySelector(selector);
    this.close_data();
  }

  show() {
    this.elem.classList.toggle("is-active");
    this.on_show();
  }

  close() {
    this.elem.classList.toggle("is-active");
    this.on_close();
  }

  close_data() {
    var modalClose = this.elem.querySelectorAll(
      "[data-bulma-modal='close'], .modal-background"
    );
    var that = this;
    modalClose.forEach(function(e) {
      e.addEventListener("click", function() {
        that.elem.classList.toggle("is-active");

        var event = new Event("modal:close");

        that.elem.dispatchEvent(event);
      });
    });
  }

  on_show() {
    var event = new Event("modal:show");

    this.elem.dispatchEvent(event);
  }

  on_close() {
    var event = new Event("modal:close");

    this.elem.dispatchEvent(event);
  }

  addEventListener(event, callback) {
    this.elem.addEventListener(event, callback);
  }
}

var btn = document.querySelector("#btn");
var mdl = new BulmaModal("#myModal");

btn.addEventListener("click", function() {
  mdl.show();
});

mdl.addEventListener("modal:show", function() {
  console.log("opened");
});

mdl.addEventListener("modal:close", function() {
  console.log("closed");
});

// Modal report
$(document).ready(function() {
  // feather.replace();
  $(".is-save").on("click", function() {
    $(this).addClass("is-loading");
    setTimeout(function() {
      $(".is-save")
        .removeClass("is-loading")
        .addClass("is-disabled");
      $(".form-wrapper, .success-block").toggleClass("is-hidden");
    }, 2000);
    setTimeout(function() {
      $(".success-block").addClass("is-active");
    }, 2500);

    setTimeout(function() {
      $(".reset").removeClass("is-hidden");
    }, 3000);
  });

  $(".reset").on("click", function() {
    $(this).addClass("is-hidden");
    $(".is-save").removeClass("is-disabled");
    $(".form-wrapper, .success-block").toggleClass("is-hidden");
    $(".success-block").removeClass("is-active");
  });
});

// let tabs = document.querySelectorAll(".tab");
// let deactvateAllTabs = function() {
//   tabs.forEach(function(tab) {
//     tab.classList.remove("is-active");
//   });
// };
// tabs.forEach(function(tab) {
//   tab.addEventListener("click", function() {
//     tab.classList.add("is-active");
//   });
// });

// $(document).ready(function() {
//   $(".tab").click(function() {
//     if ($(this).hasClass("is-active")) {
//       $(this).switchClass("is-active", "tab");
//     } else {
//       $(this).switchClass("tab", "is-active");
//     }
//   });
// });

//current position
var pos = 0;
//number of slides
var totalSlides = $("#slider-wrap ul li").length;
//get the slide width
var sliderWidth = $("#slider-wrap").width();

$(document).ready(function() {
  /*****************
	 BUILD THE SLIDER
	*****************/
  //set width to be 'x' times the number of slides
  $("#slider-wrap ul#slider").width(sliderWidth * totalSlides);

  //next slide
  $("#next").click(function() {
    slideRight();
  });

  //previous slide
  $("#previous").click(function() {
    slideLeft();
  });

  /*************************
	 //*> OPTIONAL SETTINGS
	************************/
  //automatic slider
  var autoSlider = setInterval(slideRight, 3000);

  //for each slide
  $.each($("#slider-wrap ul li"), function() {
    //set its color
    var c = $(this).attr("data-color");
    $(this).css("background", c);

    //create a pagination
    var li = document.createElement("li");
    $("#pagination-wrap ul").append(li);
  });

  //counter
  countSlides();

  //pagination
  pagination();

  //hide/show controls/btns when hover
  //pause automatic slide when hover
  $("#slider-wrap").hover(
    function() {
      $(this).addClass("active");
      clearInterval(autoSlider);
    },
    function() {
      $(this).removeClass("active");
      autoSlider = setInterval(slideRight, 3000);
    }
  );
}); //DOCUMENT READY

/***********
 SLIDE LEFT
************/
function slideLeft() {
  pos--;
  if (pos == -1) {
    pos = totalSlides - 1;
  }
  $("#slider-wrap ul#slider").css("left", -(sliderWidth * pos));

  //*> optional
  countSlides();
  pagination();
}

/************
 SLIDE RIGHT
*************/
function slideRight() {
  pos++;
  if (pos == totalSlides) {
    pos = 0;
  }
  $("#slider-wrap ul#slider").css("left", -(sliderWidth * pos));

  //*> optional
  countSlides();
  pagination();
}

/************************
 //*> OPTIONAL SETTINGS
************************/
function countSlides() {
  $("#counter").html(pos + 1 + " / " + totalSlides);
}

function pagination() {
  $("#pagination-wrap ul li").removeClass("active");
  $("#pagination-wrap ul li:eq(" + pos + ")").addClass("active");
}
