<?php 
// app/config/packages/captcha.php
if (!class_exists('CaptchaConfiguration')) { return; }

// BotDetect PHP Captcha configuration options
return [
    // Captcha configuration for example form
    'CaptchaUserRegistration' => [
        'UserInputID' => 'captchaCodeUser',
        'ImageWidth' => 250,
        'ImageHeight' => 50,
    ],
    'CaptchaContact' => [
        'ContactInputID' => 'captchaCode',
        'ImageWidth' => 250,
        'ImageHeight' => 50,
    ],
];